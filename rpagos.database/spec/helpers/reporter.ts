import { green } from 'chalk';
import { SpecReporter } from 'jasmine-spec-reporter';

let reporter = new SpecReporter({
    suite: {
        displayNumber: true
    },
    summary: {
        displayDuration: true,
        displayFailed: true, 
        displaySuccessful: true,
        displayPending: true,
        displayErrorMessages: true
    },
    colors :{
        successful: 'green',
        failed: 'red',
        pending: 'yellow',
        enabled: true
    }
})

jasmine.getEnv().clearReporters()
jasmine.getEnv().addReporter(reporter);
