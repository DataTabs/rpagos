import { Router } from 'express';
import { findClienteByPk, findClientByName, findClientByCode } from '../../controllers/find/rpClientes-find.controller';
import { createRpCliente } from '../../controllers/create/rpClientes-create.controller';
import { updateClienteAndExtra } from '../../controllers/update/rpClientes-update.controller';
import { deleteClienteAndExtra, deleteClienteByPk } from '../../controllers/delete/rpClientes-delete.controller';

export const rpClientesRoutes = (routes: Router) => {

    // TODAS LAS RUTAS DE RPCLIENTES

    /**
     * 
     * GET
     */
    routes.get('/find-cliente-by-pk/:id', findClienteByPk);
    routes.get('/find-cliente-by-name/:name', findClientByName);
    routes.get('/find-cliente-by-code/:code', findClientByCode);

    /**
     * 
     * POST
     */
    routes.post('/create-cliente-and-extra', createRpCliente);

    /**
     * 
     * PUT
     */
    routes.put('/update-cliente-and-extra/', updateClienteAndExtra);

    /**
     * 
     * DELETE
     */
    routes.delete('/delete-cliente-by-pk/:id', deleteClienteByPk);
    routes.delete('/delete-cliente-and-extra/:id', deleteClienteAndExtra);
}