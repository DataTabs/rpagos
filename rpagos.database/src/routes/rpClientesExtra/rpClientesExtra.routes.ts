import { Router } from 'express';
import { createRpClienteExtra } from '../../controllers/create/rpClientesExtra-create.controller';
import { deleteClienteExtraInfo } from '../../controllers/delete/rpClientesExtra-delete.controller';
import { findRpClientesExtraByTipoDato, 
         findRpClientesExtraByDescrip, 
         findRpClientesExtraByStatus 
       } from '../../controllers/find/rpClientesExtra-find.controller';
import { updateRpClientesExtraInfo } from '../../controllers/update/rpClientesExtra-update.controller';

export const rpClientesExtraRoutes = (routes: Router) => {

    // TODAS LAS RUTAS DE RPCLIENTESEXTRA
    /**
     * 
     * GET
     */
    routes.get('/find-cliente-extra-by-tipodato/:tipoDato', findRpClientesExtraByTipoDato);
    routes.get('/find-cliente-extra-by-descrip/:descrip', findRpClientesExtraByDescrip);
    routes.get('/find-cliente-extra-by-status/:status', findRpClientesExtraByStatus);
    
    /**
     * 
     * POST
     */
    routes.post('/create-cliente-extra', createRpClienteExtra);

    /**
     * 
     * PUT
     */
    routes.put('/update-cliente-extra', updateRpClientesExtraInfo);

    /**
     * 
     * DELETE
     */
    routes.delete('/delete-cliente-extra/:id', deleteClienteExtraInfo);
}