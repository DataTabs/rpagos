import { Router } from 'express';
import { rpClientesRoutes } from './rpClientes/rpClientes.routes';
import { rpClientesExtraRoutes } from './rpClientesExtra/rpClientesExtra.routes';
import { rpCuentaRoutes } from './rpCuenta/rpCuenta.routes';
import { rpFacturasRoutes } from './rpFacturas/rpFacturas.routes';

const routes = Router();

/**
 * 
 * rpClientes
 */
rpClientesRoutes(routes);

 /**
  * 
  * rpClientesExtra
  */
rpClientesExtraRoutes(routes);

/**
 * 
 * rpCuenta
 */
rpCuentaRoutes(routes);

/**
 * 
 * rpFacturas
 */
rpFacturasRoutes(routes);

export default routes;
