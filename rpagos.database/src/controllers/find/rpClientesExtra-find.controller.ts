import { Request, Response } from 'express';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model';

export const findRpClientesExtraByTipoDato = async (req: Request, res: Response) => {
    const tipoDato = req.params.tipoDato;
    try {
        const result = await RPCLIENTESEXTRA.findAll({ where: { TipoDato: tipoDato } });
        if( result ) {
            res.send(result).status(200);
            return;
        }
        res.send(result).status(200);
    } catch (error) {
        console.error(error);
        res.send(error);
    }
}

export const findRpClientesExtraByDescrip = async (req: Request, res: Response) => {
    const descrip = req.params.descrip;
    try {
        const result = await RPCLIENTESEXTRA.findAll({ where: { Descrip: descrip } });
        if( result ) {
            res.send(result).status(200);
            return;
        }
        res.send(result).status(200);
    } catch (error) {
        console.error(error);
        res.send(error);
    }
}


export const findRpClientesExtraByStatus = async (req: Request, res: Response) => {
    const status = req.params.status;
    try {
        const result = await RPCLIENTESEXTRA.findAll({ where: { Status : status } });
        if( result ) {
            res.send(result).status(200);
            return;
        }
        res.send(result).status(200);
    } catch (error) {
        console.error(error);
        res.send(error);
    }
}