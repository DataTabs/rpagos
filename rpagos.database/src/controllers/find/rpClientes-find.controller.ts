import { Request, Response } from 'express';
import { RPCLIENTES } from '../../model/models/rpClientes.model';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model'; 
import { RpClientes } from '../../model/interfaces/rpClientes.interface';

export const findClienteByPk = async (req: Request, res: Response) => {
    const clienteId: string = req.params.id;
    try {
        const result = await RPCLIENTES.findAll(
            { where: {
                idCliente: clienteId,
            }, include: RPCLIENTESEXTRA})
        if( result ) {
            res.send(result).status(200);
            return;
        }
        res.send({
            exist: false
        });
    } catch (error) {
        res.send({
            error
        });
    }
}

export const findClientByName  = async ( req: Request, res: Response ) => {
    const clientName: string = req.params.name;
    try {
        const result = await RPCLIENTES.findAll({
            where: { ClienteName: clientName },
            include: RPCLIENTESEXTRA
        });
        if( result ) {
            res.send(result);
            return;
        }
        res.send(result);
    } catch (error) {
        res.send(error);
    }
}

export const findClientByCode = async (req: Request, res: Response) => {
    const clientCode: string = req.params.code;
    try {
        const result = await RPCLIENTES.findAll({
            where: { CodeCliente: clientCode },
            include: RPCLIENTESEXTRA
        });
        if( result ) {
            res.send(result);
            return;
        }
        res.send(result);
    } catch (error) {
        res.send(error);
    }
}
