import { Request, Response } from 'express';
import { RPCLIENTES } from '../../model/models/rpClientes.model';
import { RpClientes } from '../../model/interfaces/rpClientes.interface';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model';

export const createRpCliente = async (req: Request, res: Response) => {
    const cliente: RpClientes = req.body;
    try {
        // BUSCO EL REGISTRO PARA VER SI EXISTE 

        const result = await RPCLIENTES.findByPk(cliente.idCliente);
        // SI EL RESGISTRO NO EXISTE LO CREA
        if( !result ) {
            try {
                const result_ = await RPCLIENTES.create(cliente , {include: [ RPCLIENTESEXTRA ]});
                res.send({
                    created: true,
                    exist: false,
                    result_
                }).status(201);
                return;
            } catch (error) {
                // ERROR AL CREAR EL REGISTRO
                res.send({
                    created: false,
                    error
                }).status(200);
            }
        }
        // EL REGISTRO YA EXISTE
        res.send({
            exist: true,
            created: false
        });
    } catch (error: any) {
        // ERROR EN VERIFICAR EL REGISTRO
        res.send({
            created: false,
            error
        }).status(200);
    }

}


