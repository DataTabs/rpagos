import { Request, Response } from 'express';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model';
import { RpClientesExtra } from '../../model/interfaces/rpClientesExtra.interface';

export const createRpClienteExtra = async (req: Request, res: Response) => {
    const clienteExtra: RpClientesExtra = req.body;
    try {
        // BUSCO EL REGISTRO PARA VER SI EXISTE
        const result =  await RPCLIENTESEXTRA.findByPk(clienteExtra.idExtraCliente);

        // SI EL RESGISTRO NO EXISTE LO CREA
        if( !result ) {
            try {
                const result = await RPCLIENTESEXTRA.create(clienteExtra);
                res.send({
                    created: true,
                    exist: false,
                    result
                }).status(201);
                return;    
            } catch (error) {
                // ERROR AL CREAR EL REGISTRO
                res.send({
                    created: false,
                    error
                }).status(200);
            }
        }
          // EL REGISTRO YA EXISTE
          res.send({
            exist: true,
            created: false
        });
    } catch (error) {
         // ERROR EN VERIFICAR EL REGISTRO
         res.send({
            created: false,
            error
        }).status(200);
    }
}