import { Request, Response } from 'express';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model';


export const updateRpClientesExtraInfo = async (req: Request, res: Response) => {
    const extras = req.body;
    try {
        const result_ = await RPCLIENTESEXTRA.bulkCreate(extras, { updateOnDuplicate: ['TipoDato', 'Descrip', 'Status'] })
        if(result_) {
            res.send({
                ...result_
                }
            ); 
            return;
        }
    } catch (error) {
        res.send({
            error
        });
    }
}