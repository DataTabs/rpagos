import { Request, Response } from 'express';
import { Op } from 'sequelize';
import { RPCLIENTES } from '../../model/models/rpClientes.model';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model';
import { RpClientes } from '../../model/interfaces/rpClientes.interface';
import { RpClientesExtra } from '../../model/interfaces/rpClientesExtra.interface';

export const updateClienteAndExtra = async (req: Request, res: Response) => {
    const { extras, ...cliente } = req.body;
    try {
        // ACTUALIZA UN SOLO CLIENTE
        const result =  await RPCLIENTES.update(cliente, { where: { idCliente: cliente.idCliente} });
        // ACTUALIZA TODOS LOS EXTRA DE ESE CLIENTE S
        const result_ = await RPCLIENTESEXTRA.bulkCreate(extras, { updateOnDuplicate: ['TipoDato', 'Descrip', 'Status'] })
        
        if( result_ && result ) {
            res.send({
                result,
                ...result_
            }
            );
            return;
        }
        
    } catch (error) {
        res.send({
            error
        });
    }

}