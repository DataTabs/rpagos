import { Request, Response } from 'express';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model';

export const deleteClienteExtraInfo =  async (req: Request, res: Response) => {
    const idExtraCliente =  req.params.id;
    try {
        const resultClienteExtra =  await RPCLIENTESEXTRA.destroy({ where: { idExtraCliente: idExtraCliente } });
        if( resultClienteExtra > 0 ) {
            res.send({
                clientesExtraDeleted: resultClienteExtra,
                registerFound:  true
            }).status(200);
            return;
        }
        res.send({
            clientesExtraDeleted: resultClienteExtra,
            registerFound:  false
        }).status(200);
    } catch (error) {
        res.send({
            error
        });
    }
}