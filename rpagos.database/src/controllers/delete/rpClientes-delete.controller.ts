import { Request, Response } from 'express';
import { RPCLIENTES } from '../../model/models/rpClientes.model';
import { RPCLIENTESEXTRA } from '../../model/models/rpClientesExtra.model';


export const deleteClienteAndExtra =  async (req: Request, res: Response) => {
    const clienteId = req.params.id;
    try {
        // VERIFICAR SI EL CLIENTE TIENE FACTURAS 
        // SI TIENE NO SE ELIMINAN LOS DATOS 
        // SI NO TIENE SE ELIMINAN LOS DATOS
        const resultCliente = await RPCLIENTES.destroy({ where: { idCliente: clienteId } });
        const resultClienteExtra =  await RPCLIENTESEXTRA.destroy({ where: { idCliente: clienteId }});
        if( resultCliente > 0 && resultClienteExtra > 0 ) {
            res.send({
                clientesDeleted: resultCliente,
                clientesExtraDeleted: resultClienteExtra,
                registerFound:  true
            }).status(200);
            return;
        }
        res.send({
            clientesDeleted: resultCliente,
            clientesExtraDeleted: resultClienteExtra,
            registerFound: false
        })    
    } catch (error) {
        res.send({
            error
        });
    }
}


export const deleteClienteByPk = async (req: Request, res: Response) => {
    const clienteId = req.params.id;
    try {
        const resultCliente = await RPCLIENTES.destroy({ where: { idCliente: clienteId } });
        if( resultCliente > 0 ) { 
            res.send({
                clientesDeleted: resultCliente,
                registerFound:  true
            }).status(200);
            return;
        }
        res.send({
            clientesDeleted: resultCliente,
            registerFound: false
        })
    } catch (error) {
        res.send({
            error
        });
    }
}