export interface RpClientesExtra {
    idExtraCliente: number;
    idCliente: number;
    TipoDato: number;
    Descrip: string;
    Status: boolean;
}