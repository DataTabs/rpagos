export interface RpClientes {
    idCliente: number;
    CodeCliente: string;
    ClienteName: string;
    StatusC: number;
    idPais: number;
    idCiudad: number;
}