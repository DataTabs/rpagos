import sequelize from '../../database/database';
import { DataTypes } from 'sequelize';
import { RPCLIENTESEXTRA } from './rpClientesExtra.model';

export const RPCLIENTES = sequelize.define('clientes', {
    idCliente: {
        type: DataTypes.INTEGER({
            length: 11
        }),
        primaryKey: true,
        autoIncrement: true
    },
    CodeCliente: {
        type: DataTypes.STRING(60),
        allowNull: false
    },
    ClienteName: {
        type: DataTypes.STRING(120),
        allowNull: true
    },
    StatusC: {
        type: DataTypes.SMALLINT({
            length: 2,
        }),
        allowNull: true
    },
    idPais: {
        type: DataTypes.SMALLINT({
            length: 2
        }),
        allowNull: true
    },
    idCiudad: {
        type: DataTypes.SMALLINT({
            length: 2
        }),
        allowNull: true
    }
}, { tableName: 'rpClientes' });

RPCLIENTES.hasMany(RPCLIENTESEXTRA, { foreignKey: 'idCliente' });