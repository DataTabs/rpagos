import sequelize from '../../database/database';
import { DataTypes } from 'sequelize';
import { RPCLIENTES } from './rpClientes.model';

export const RPCLIENTESEXTRA = sequelize.define('extra', {
    idExtraCliente: {
        type: DataTypes.INTEGER({
            length: 11
        }),
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    idCliente: {
        type: DataTypes.INTEGER({
            length: 11
        }),
        references: {
            model: RPCLIENTES,
            key: 'idCliente'
        },
        primaryKey: true,
        allowNull: false,
    },
    TipoDato: {
        type: DataTypes.SMALLINT({
            length: 2
        }),
        allowNull: true
    },
    Descrip: {
        type: DataTypes.STRING(145),
        allowNull: true
    },
    Status: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    }
}, { tableName: 'rpClientesExtra', freezeTableName: true});