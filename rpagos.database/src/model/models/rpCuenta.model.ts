import sequelize from '../../database/database';
import { DataTypes } from 'sequelize';

export const RPCUENTA = sequelize.define('rpCuenta', {
    idCuenta: {
        type:  DataTypes.INTEGER({
            length: 11
        }),
        primaryKey: true,
        autoIncrement: true
    },
    Name: {
        type: DataTypes.STRING(80),
        allowNull: false
    }
});