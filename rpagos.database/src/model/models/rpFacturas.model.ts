import sequelize from '../../database/database';
import { DataTypes } from 'sequelize';

export const RPFACTURAS = sequelize.define('rpFacturas', {
    idFacturas: {
        type: DataTypes.INTEGER({
            length: 11
        }),
        primaryKey: true,
        autoIncrement: true
    },
    CodeCliente: {
        type: DataTypes.STRING(60),
        allowNull: false
    },
    CodeUser: {
        type: DataTypes.STRING(60),
        allowNull: false,
    },
    NumFactura: {
        type: DataTypes.STRING(20),
        references: {
            // referencia a otra tabla y campo
        },
        allowNull: false
    },
    NumControl: {
        type: DataTypes.STRING(20),
        allowNull: true
    },
    FechaFactura: {
        type: DataTypes.DATE,
        allowNull: false
    }
})