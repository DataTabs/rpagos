import express, { Application } from 'express';
import cors from 'cors';
import db from '../database/database';
import routesApi from '../routes/routes';
import chalk from 'chalk';

export class Server {
    private app: Application;
    private port: string = process.env.PORT || '5056';
    private apiPath = {
        routes: '/api'
    }

    constructor() {
        this.app = express();
        this.middlewares();
        this.routes();
		this.dbConnection();
    }

    middlewares() {

		// CORS
		this.app.use( cors() );

		// Parse JSON from request
		this.app.use( express.json() );
	}

	routes() {
		this.app.use( this.apiPath.routes,  routesApi );
	}

	async dbConnection() {
		try {
			await db.authenticate().then(() => {
				console.log(chalk.italic.magentaBright('DATABASE CONNECTION SUCCESSFULL 💯  💯'));
			});
		} catch (error) {
			console.error(error);
			console.log('No pudo contentarse a la BD');
		}
	}

	listen() {
		this.app.listen( this.port, () => {
			console.log( chalk.blueBright(`SERVER EXPRESS ONLINE PORT: ${this.port}`));
		});
	}

}