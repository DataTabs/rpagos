import { Sequelize } from 'sequelize';
require('dotenv').config();

const DATABASE = process.env.DATABASE || '';
const DB_USER = process.env.DB_USER || '';
const DB_PASSWORD = process.env.DB_PASSWORD || '';
const HOST = process.env.HOST || '';

const db = new Sequelize(DATABASE, DB_USER, DB_PASSWORD,{
    dialect: 'mysql',
    host: HOST,
    logging: false
});

export default db;