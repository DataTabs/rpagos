const express = require('express');
const app = express(); 
"use strict";
const nodemailer = require("nodemailer");
const { rejects } = require('assert');
const { resolve } = require('path');
const path = require('path');
const fs = require("fs");

function enviarHtml(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoice-paid/invoice-paid.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml1(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoice-paid-part/invoice-paid-part.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml2(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/invoice.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml3(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/invoice1.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml4(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/invoice2.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml5(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/invoice3.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml6(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/invoice4.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml7(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/ejemplos/example1/index.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml8(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/ejemplos/example1/example2-paid-fully.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml9(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/ejemplos/example1/example2-paid-part.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml10(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/ejemplos/example2/index.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml11(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/ejemplos/example2/paid-part2.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml12(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/ejemplos/example2/paid-fully.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml13(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/invoices/ejemplos/example3/index.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml14(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/many-invoices/many-invoices.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml15(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/reset-password/reset-password.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml16(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/verify-email/verify-email.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};
function enviarHtml17(){
  return new Promise((resolve,reject)=>{
    invoice = "../remote-mailer/welcome/welcome.html";
    fs.readFile(invoice, 'utf8', (error, datos) => {      
        if (error) reject(error);
        resolve(datos);
    });  
  })
};

app.listen(3000, () => {
  console.log('Server on port 3000');
});

// async..await is not allowed in global scope, must use a wrapper
async function  main() {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();


  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'thomastalk.me@gmail.com', // generated ethereal user
      pass: '', // generated ethereal password
    },
  });

  enviarHtml().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml1().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml2().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml3().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml4().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml5().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml6().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml7().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml8().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml9().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml10().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml11().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml12().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml13().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml14().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml15().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml16().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  enviarHtml17().then( async datos => {
    // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"TuFactura" <thomastalk.me@gmail.com>', // sender address
    to: "pestanaxd99@gmail.com", // list of receivers
    subject: "TuFactura digital", // Subject line
    html: datos, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  });
  
}

main().catch(console.error);
