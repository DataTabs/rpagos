import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { sofboxModule } from './sofbox/sofbox.module';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ContactFormService } from './sofbox/components/contact-form/contact-form.service';
import { SaasMainModule } from './saas-main/saas-main.module';
import { SaasTwoModule } from './saas-two/saas-two.module';
import { SaasThreeModule } from './saas-three/saas-three.module';
import { SaasFourModule } from './saas-four/saas-four.module';
import { SaasFiveModule } from './saas-five/saas-five.module';
import { SaasSixModule } from './saas-six/saas-six.module';

//Traductor
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/','.json');
}


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    sofboxModule,    
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),  
    RecaptchaModule,
    SaasMainModule,
    SaasTwoModule,
    SaasThreeModule,
    SaasFourModule,
    SaasFiveModule,
    SaasSixModule

  ],
  providers: [
    ContactFormService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
