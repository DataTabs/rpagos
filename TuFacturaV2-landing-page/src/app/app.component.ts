import { Component } from '@angular/core';
import { PluginsService } from './sofbox/plugins.service';
import { Router, NavigationEnd} from "@angular/router";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private plugins: PluginsService, private router: Router, public translate: TranslateService) {
    translate.addLangs(['en', 'es', 'pr']);
    translate.setDefaultLang('pr');
    translate.setDefaultLang('es');
   }

  title = 'sofbox-angular';

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }
  public get currentLanguage(): string {
    return this.translate.currentLang;
   }
}
