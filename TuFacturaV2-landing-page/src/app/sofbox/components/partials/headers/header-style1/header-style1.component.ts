import {Component, Input, OnInit,ElementRef, ViewChild, HostListener} from '@angular/core';
import $ from 'jquery';

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-header-style1',
  templateUrl: './header-style1.component.html',
  styleUrls: ['./header-style1.component.css']
})
export class HeaderStyle1Component implements OnInit { 

  track($event) {
  console.debug("Scroll Event", $event);
  }

  @ViewChild('div', {static: true}) _div: ElementRef;

  @Input() logoImg: string;

  @Input() navItemList: any[];

  @Input() className: string;

  @Input() styledLogo = true;

  constructor(public translate: TranslateService) {  
    translate.addLangs(['en','es','pr']);
   };

  mClase: string = 'color-transparent';  

  scroll = (event): void => {
    if (event.path[1].scrollY > 50)  this.mClase = 'color-nav'
    else this.mClase = 'color-transparent'
  };

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);       
  }    

  changeLanguage(language: string){
    this.translate.use(language);
  }

  jumpTo(href) {
    $('html, body').stop().animate({
      scrollTop: $(href).offset().top
    }, 1500);
  }

};