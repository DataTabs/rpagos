import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  Prefijo: string = 'About.'

  constructor() { }

  public cardData = [

    {
      title: 'Software',
      icon: 'ion-ios-monitor-outline',
      description: 'SistemaFactura'
    },
    {
      title: 'FacturacionMasiva',
      icon: 'ion-ios-albums-outline', 
      description: 'DocumentosTributarios'
    },
    {
      title: 'NotasDeCreditos',
      icon: 'ion-ios-paper-outline',
      description: 'DocumentosTributarios'
    },
    {
      title: 'SistemaDeCobranza',
      icon: 'ion-cash',
      description: 'SistemaCobranza'
    },
  
  ];

  ngOnInit() {
  }

}
