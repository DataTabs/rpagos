import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
// @ts-ignore
import logoImg from '../../../../assets/images/logo-footer.png';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  Prefijo: string = 'Footer.'

  public navLogo: string = logoImg;

  public footerText: any = [
    {
      icon: true,
      iconClass: 'ion-ios-location-outline',
      text: 'Edif. Centro Solano Plaza I, Piso 4, Oficina 4A., Urb. Sabana Grande. Caracas 1050, Venezuela.'
    },
    {
      icon: true,
      iconClass: 'ion-ios-telephone-outline',
      text: '+58 212 111-1111'
    },
    {
      icon: true,
      iconClass: 'ion-ios-email-outline',
      text: 'mail@tufactura.com'
    }
  ];

  public footerHome: any = [
    {
      section: [
        {
          href: '#',
          title: 'Inicio'
        },
        {
          href: '#about-us',
          title: 'Nosotros'
        },
        {
          href: '#feature',
          title: 'Servicios'
        },
        {
          href: '#testimonial',
          title: 'Sistema'
        },
        {
          href: '#our',
          title: 'Tarifas'
        }
      ]
    }
  ];

  public footerTiming: any = [
    {
      icon: true,
      text: 'Lunes - Viernes: 09AM - 05PM'
    },
    {
      icon: true,
      text: 'Sábado: 09AM - 01PM'
    }
  ];

  // @ts-ignore

  public socialNavItems: any = [
    { href: 'https://twitter.com/', icon: '<i class="fab fa-twitter"></i>' },
    { href: 'https://www.facebook.com/', icon: '<i class="fab fa-facebook-f"></i>' },
    { href: 'https://mail.google.com/', icon: '<i class="fab fa-google"></i>' }
  ];

  constructor() { }
  private translate: TranslateService

  ngOnInit() {
  }

}
