import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PluginsService} from '../../sofbox/plugins.service';
import { subHeader } from '../../../constants/menu';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class IndexComponent implements OnInit {

  Prefijo: string = 'Home.'
  confirmDialogRef: any;

  constructor(private plugins: PluginsService,
    private translate: TranslateService) {
  }

  public navItems: any = [
    { href: '#about-us', title: 'Nosotros' },
    { href: '#feature', title: 'Servicios' },
    { href: '#our', title: 'Tarifas' },
    { href: '#testimonial', title: 'Sistema' },
    { href: '#get-in-tech', title: 'Contactos' },
    {
      href: '#', title: 'ES',
      _is_active: true,
      children: true,
      child: [
        { href: '#', title: 'EN' },
        { href: '#', title: 'PR' }
      ]
    },
  ];

  public footerText = 'Desarrollo';

  public socialNavItems: any = [
    { href: 'javascript:void(0)', icon: '<i class="fab fa-twitter"></i>' },
    { href: 'javascript:void(0)', icon: '<i class="fab fa-facebook-f"></i>' },
    { href: 'javascript:void(0)', icon: '<i class="fab fa-google"></i>' },
    { href: 'javascript:void(0)', icon: '<i class="fab fa-github"></i>' },
  ];

  ngOnInit() {
    // Init all plugins...
    const current = this;
    // tslint:disable-next-line:only-arrow-functions
    setTimeout(function () {
      current.plugins.index();
    }, 200);
  }

}
