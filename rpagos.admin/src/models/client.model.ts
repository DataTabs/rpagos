export interface client{
    idCliente?:string|number
    idEmpresa:number
    CodeCliente?:string //Esto se crea desde el front
    idTipoClientes?:number //TODO Falta hacer un select
    ClienteName:string
    name?:string //TODO: Falta en bd el nombre del responsable
    rif_nit: string
    avatar: string
    StatusC:number
    idPais:number
    idCiudad:number
    rpClientesExtras:[extras?]
    nota:string
}

export interface extras{
    idCliente:string
    idExtraCliente:string
    TipoDato:tipoDato
    Descrip:string
    StatusC:number
    ExtraTipoDato?:string
}

export enum tipoDato{
    Direccion = 1,
    Email = 2,
    Telefonos = 3,
    Redes_Sociales = 4,
}
