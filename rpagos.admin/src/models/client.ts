export interface Client {   
    id: string;
    name: string;
    email: string;
    phoneNumber?: {
        code: string;
        phoneNumber: string;
    }[];
    company?: string;
    country?: string;
}