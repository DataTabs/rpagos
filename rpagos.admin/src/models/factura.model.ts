export interface Factura{
    header:{
        empresa:Empresa
        bill:Bill
        info:Info
    },

    content:{
        services:[Service]
        taxes?:[Tax]
        Discounts?:[Discount]
    },

    footer?:{
        subTotal?:Number
        taxTotal?:Number
        discountTotal?:Number
        total?:Number
        Message?:String
        AdditionalInfo?:String
    },

}

interface Info{
    fact_number:String
    Date:Date
    DueDate:Date
    Total_Due:Number
}

interface Empresa{
    name:String
    address1:String
    address2?:String
    phone:Number
    email:String
    pag:String
    logo:String
}


interface Bill{
    name:String
    address1:String
    address2?:String
    phone:Number
    email:String
}


interface Service{
    name:String
    description?:String
    price:Number
    qty?:Number
}


interface Tax{
    name:String
    description?:String
    amount?:Number
}


interface Discount{
    name:String
    description?:String
    amount?:Number
}
