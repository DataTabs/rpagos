export interface Facturas {
    idControl: string,
    idFactura: string,
    idCliente: string,
    Direccion: string,
    paginaWeb: string,
    Rif_Nit: string,
}
