import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExtraOptions, PreloadAllModules, RouterModule } from '@angular/router';
import { MarkdownModule } from 'ngx-markdown';
import { FuseModule } from '@fuse';
import { FuseConfigModule } from '@fuse/services/config';
import { FuseMockApiModule } from '@fuse/lib/mock-api';
import { CoreModule } from 'app/core/core.module';
import { appConfig } from 'app/core/config/app.config';
import { mockApiServices } from 'app/mock-api';
import { LayoutModule } from 'app/layout/layout.module';
import { AppComponent } from 'app/app.component';
import { appRoutes } from 'app/app.routing';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataMemoryService } from './services/InMemory/data-memory.service';
import { MatSelectCountryModule } from "@angular-material-extensions/select-country";

//import { Error401Component } from './modules/admin/pages/error/error401/error401.component';
//import { UsersComponent } from './modules/admin/apps/users/users.component';
//import { PermissionsComponent } from './modules/admin/apps/permissions/permissions.component';
//import { RolesComponent } from './modules/admin/apps/roles/roles.component';

const routerConfig: ExtraOptions = {
    scrollPositionRestoration: 'enabled',
    preloadingStrategy       : PreloadAllModules
};

@NgModule({
    declarations: [
        AppComponent,
        //Error401Component,
        //UsersComponent,
        //PermissionsComponent,
        //RolesComponent,
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        MatSelectCountryModule.forRoot('es'),
        RouterModule.forRoot(appRoutes, routerConfig),

        // Fuse, FuseConfig & FuseMockAPI
        FuseModule,
        FuseConfigModule.forRoot(appConfig),
        FuseMockApiModule.forRoot(mockApiServices),

        // Core module of your application
        CoreModule,

        // Layout module of your application
        LayoutModule,

        // 3rd party modules that require global configuration via forRoot
        MarkdownModule.forRoot({}),

        //Juan
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(DataMemoryService, {delay:0, passThruUnknownUrl:true})
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
