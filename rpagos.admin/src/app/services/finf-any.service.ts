import { Injectable } from '@angular/core';
import { AuthService } from 'app/core/auth/auth.service';
import { Observable } from 'rxjs';
import { GlobalApiService } from './request/global-api.service';

@Injectable({
  providedIn: 'root'
})
export class FinfAnyService {

  public table = ""
  public Campo = ""
  public Valor = 0

  constructor(
    private globalServiceApi: GlobalApiService,
    private _AuthService: AuthService
  ) { }

  getAllData(): Observable<any> {
    const { idCuenta } = this._AuthService.getDecodedAccessToken()
    return this.globalServiceApi.makeGetRequest(`find-any-info/${this.table}/${this.Campo}/${this.Valor}`);
  }

}
