import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataMemoryService implements InMemoryDbService {

  DataCreate: any;
  ArgDB: any[] = ["Table1", "Table2", "Table3"];
  StrArgDB: any[] = [];
  

  constructor(/*public _HttpClient: HttpClient*/) {

    let TemStr = "{"
    this.ArgDB.forEach(element => {
      TemStr = TemStr + element + ": [],"

    });
    TemStr = TemStr + "}"
    this.DataCreate = JSON.stringify(TemStr)

    //console.log(this.DataCreate)
    //this.AddItem("Factura", { id: 11, name: 'Dr Nice' })
  }



  createDb() {
    //console.log(JSON.stringify(this.DataCreate))
    return {
      Table1: [{ item: { id: "99999999999999" }, id: "99999999999999"  }],
      Table2: [{ item: { id: "99999999999999" }, id: "99999999999999"  }],
      Table3: [{ item: { id: "99999999999999" }, id: "99999999999999"  }]
    }
  }


  /*AddItem(DBName: string, Data: any) {

    // const Encontre = this.StrArgDB.find(item => item === DBName);
    const Encontre = this.StrArgDB.findIndex(item => item === DBName);

    if (Encontre != -1) {
      const MyAPi = this.ArgDB[Encontre]
      console.log(MyAPi)

      //this._HttpClient.post('api/' + MyAPi, Data).subscribe((response: any) => { }, (Error) => { console.error('Users'); console.error(Error) });

    } else {
      this.StrArgDB.push(DBName)
      const MyAPi = this.ArgDB[this.StrArgDB.length - 1]
      console.log(MyAPi)
    }

  }*/

}

