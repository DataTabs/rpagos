import { Injectable } from '@angular/core';
import { GlobalApiService }  from '../request/global-api.service';

@Injectable({
  providedIn: 'root'
})
export class HashService {

  constructor( private globalApi: GlobalApiService ) { }

  hashPassaword(valueToHash: string) {
    return this.globalApi.makeGetRequest(`hash-pass-user/${valueToHash}`);
  }

}
