import { Injectable } from '@angular/core';
import { client } from 'models/client.model';

@Injectable({
  providedIn: 'root'
})
export class BuildModelsService {

  constructor() { }

    /**
     * Recibe la data de empresa y retorna estructura de Empresa para bd
     * @param {empresa} Data desde componeten
     * @returns {Empresa} Empresa para bd
     */
  BuildEmpresatoDB(empresa){
      console.log("Antes: ",empresa)
      let newEmpresa:any={
          Nombre:empresa.name,
          Direccion:'',//TODO Falta direccion de la empresa
          Rif_nit:empresa.reasonSocial,
          email_master:empresa.email,
          idPais:empresa.country,
          LogoURL:'',
      }

      console.log("Despues: ",newEmpresa)

  }

  BuildUser(user){
      /*
      	"idCuenta": 0,
	"idRol": 0,
	"NameUser": "Elias Duarte",
	"LoginUser": "Elias Duarte",
	"PassUser": "123456789",
	"Email": "elias@gmail.com" */
      let newUser:any={
          idCuenta:0,
          idRol:0,
          NameUser:user.name,
          LoginUser:user.email,
          PassUser:user.password,
          Email:user.email,
      }
      return newUser

  }

    /**
     * Recibe la data de clientes y retorna estructura de Clientes para bd
     * @param {data} Data desde componeten
     * @returns {Cliente} Cliente para bd
     */
/*     buildClient(data: any){
        console.log(data)
          let cliente:client={
            idCliente: 0,
            idEmpresa: 0,
            CodeCliente: '',
            ClienteName: '',
            rif_nit: '',
            avatar: '',
            StatusC: 0,
            idPais: 0,
            idCiudad: 0,
            extras: [],
            nota: ''
        }
        cliente.idEmpresa=45
        cliente.CodeCliente="0123"
        cliente.idTipoCliente=3
        cliente.ClienteName=data.company
        cliente.name=data.name
        cliente.rif_nit=data.company //TODO: Cambiar este nombre de atributo
        cliente.avatar=data.avatar
        cliente.StatusC=0
        cliente.idPais=13
        cliente.idCiudad=34
        cliente.nota=data.notes

        cliente.extras.push({
            TipoDato: 1,
            Descrip:data.address,
            StatusC:0
        })

        cliente.extras.push({
            TipoDato: 1,
            Descrip:data.addressShop,
            StatusC:0
        })

        data.emails.forEach(email => cliente.extras.push({
            TipoDato: 2,
            Descrip:email.email,
            ExtraTipoDato:email.label,
            StatusC:0
        }
        ))

        data.phoneNumbers.forEach(phoneNumber => cliente.extras.push({
            TipoDato: 3,
            Descrip:phoneNumber.phoneNumber, //TODO Falta el codigo de pais
            ExtraTipoDato:phoneNumber.label,
            StatusC:0

        }))
        data.redes.forEach(redes => cliente.extras.push({
            TipoDato: 4,
            Descrip:redes.name,
            ExtraTipoDato:redes.label,
            StatusC:0
        }))

        return cliente
    } */

}
