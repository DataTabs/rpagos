import { TestBed } from '@angular/core/testing';

import { BuildModelsService } from './build-models.service';

describe('BuildModelsService', () => {
  let service: BuildModelsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BuildModelsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
