import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { result } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class DTableService {

  ArgDB: any[] = ["Table1", "Table2", "Table3"];
  StrArgDB: any[] = ['Factura'];

  constructor(private _Httpclient: HttpClient) {


  }

  AddItem(DBName: string, Data: any, tipo: String) {
    return new Promise((resolve, reject) => {
      try {

        // const Encontre = this.StrArgDB.find(item => item === DBName);

        const Encontre = this.StrArgDB.findIndex(item => item === DBName);
        let MyAPi

        if (Encontre != -1) {

          MyAPi = this.ArgDB[Encontre]

        } else {

          this.StrArgDB.push(DBName)
          MyAPi = this.ArgDB[this.StrArgDB.length - 1]

        }

        console.log('Api: ' + MyAPi)
        console.log('Opcion enlegida: ' + tipo)

        if (tipo == 'get' || '') {
          this.GetMemory(MyAPi).then((res: any) => {
            resolve(res)
          })
        }

        if (tipo == 'new') {
          this.insertMemory(MyAPi, Data).then((res: any) => {
            resolve(res)
          })
        }

        if (tipo == 'edit') {
          this.EditMemory(MyAPi, Data).then((res: any) => {
            resolve(res)
          })
        }

        if (tipo == 'delete') {
          this.DeleteMemory(MyAPi, Data.id).then((res: any) => {
            resolve(res)
          })
        }

      } catch (error) {
        console.error(error)
      }

    })

  }

  GetMemory(Api: any) {
    return new Promise((resolve, reject) => {
      try {

        this._Httpclient.get('api/' + Api).subscribe((response: any) => {
          const DataSalida = response.filter(x => x.id != "99999999999999")
          resolve(DataSalida)
        })

      } catch (error) {
        console.error(error)
      }

    })

  }

  insertMemory(Api: any, DataI: any) {
    return new Promise((resolve, reject) => {
      try {

        const Dlength = DataI.length - 1

        if (DataI.length > 0) {

          for (let index = 0; index < DataI.length; index++) {

            this._Httpclient.post('api/' + Api + '/', DataI[index]).subscribe((response: any) => { }, (Error) => { console.error('Error In Insert Memory'); console.error(Error) });

            if (index == Dlength) {
              this.GetMemory(Api).then((result: any) => {
                resolve(result)
              })
            }

          }

        } else {

          this._Httpclient.post('api/' + Api + '/', DataI).subscribe((response: any) => { this.GetMemory(Api).then((result: any) => { resolve(result) }) }, (Error) => { console.error('Error In Insert Memory'); console.error(Error) });

        }


      } catch (error) {
        console.error(error)
      }

    })


  }

  EditMemory(Api: any, DataI: any) {
    return new Promise((resolve, reject) => {
      try {

        this._Httpclient.put('api/' + Api + '/' + DataI.id, DataI).subscribe((response: any) => { this.GetMemory(Api).then((result: any) => { resolve(result) }) }, (Error) => { console.error('Error In Edit Memory'); console.error(Error) });

      } catch (error) {
        console.error(error)
      }

    })

  }

  DeleteMemory(Api: any, Id: any) {
    return new Promise((resolve, reject) => {
      try {

        this._Httpclient.delete('api/' + Api + '/' + Id).subscribe((response: any) => { this.GetMemory(Api).then((result: any) => { resolve(result) }) }, (Error) => { console.error('Error In Delete Memory'); console.error(Error) });

      } catch (error) {
        console.error(error)
      }

    })

  }

}
