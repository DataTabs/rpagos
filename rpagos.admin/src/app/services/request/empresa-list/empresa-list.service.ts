import { Injectable } from '@angular/core';
import { AuthService } from 'app/core/auth/auth.service';
import { Observable } from 'rxjs';
import { GlobalApiService } from '../global-api.service';

@Injectable({
  providedIn: 'root'
})
export class EmpresaListService {

  public EmpresasAcceso:any
  constructor(private globalServiceApi: GlobalApiService,private _AuthService:AuthService) { }

  GetAllEmpresaByidCuenta():Observable<any> {
    const { idCuenta } =  this._AuthService.getDecodedAccessToken()
    console.log(idCuenta)
    this.EmpresasAcceso = this.globalServiceApi.makeGetRequest(`find-any-info/rpEmpresas/idCuenta/${idCuenta}`);
    return this.EmpresasAcceso;
  }

  GetIdEmpresaActiva() {
    console.log(this.EmpresasAcceso)
    if (Array.isArray(this.EmpresasAcceso) && this.EmpresasAcceso.length > 0)
      return this.EmpresasAcceso[0].idEmpresa
    else
      return 0   // Se tiene que tener cuidado con esto el deber ser es hacer logout
  }

}
