import { Injectable } from '@angular/core';
import { GlobalApiService } from '../global-api.service';
import { Observable } from 'rxjs';
import { DataRelationsCreations, DataRelationsUpdate } from '../../../mock-api/relations/dataBaseRelations';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  table="rpServicios"
  private relations;
  header:any;

  constructor(private globalServiceApi: GlobalApiService ) {
   }

  // PETICIONES POST

  addServicio(servicio: any): Observable<any> {
    console.log("addServicio");
    this.relations = DataRelationsCreations.find((relation) => relation.table === this.table);
    if( this.relations === undefined ) {
      this.relations = {};
      this.relations.keysToAddId = '["idServicios"]'
      this.relations.arrayIntoObj = '[]'
      this.relations.relations = 'false';
    }

    return this.globalServiceApi.makePostRequest(`create-info/${this.table}`, servicio, {
      'x-keys-to-add-id': ''+this.relations.keysToAddId+'',
      'x-keys-of-arrays': ''+this.relations.arrayIntoObj+'',
      'x-relations': this.relations.relations
    });
  }

  // PETICONES PUT

  updateServicio(updated: any): Observable<any> {
    return this.globalServiceApi.makePutRequest(`update-info/${this.table}/idServicios`,updated,
    {
      'x-attr-duplicate': '[""]',
      'x-elements-obj': '[""]',
      'x-multiple-update': 'false'
    }
    )
  }

}
