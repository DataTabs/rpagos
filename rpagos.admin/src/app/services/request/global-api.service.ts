import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment as env } from '../../../environments/environment';
import { environment as envProd } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class GlobalApiService {

  private URLAPI = env.urlApi;
  private Token=''
  private headers: HttpHeaders = new HttpHeaders(
      {
       'x-keys-to-add-id': ["idCliente","idExtraCliente"],
       'x-keys-of-arrays':["extras"],
       'x-relations':"true"
      }
      )

  constructor( private http: HttpClient ) { }

  getData(Table: string, Campo: string, Dato: any): Observable<any> {
    return this.http.get(`${this.URLAPI}/find-any-info/${Table}/${Campo}/${Dato}`);
  }

  makeGetRequest(path: string, header?: any): Observable<any> {
    return this.http.get(`${this.URLAPI}/${path}`, { headers: header});
  }

  makePostRequest(path: string, data: any, header?): Observable<any> {
    return this.http.post(`${this.URLAPI}/${path}`, data, { headers: header});
  }

  makePutRequest(path: string, data: any, header?): Observable<any> {
    return this.http.put(`${this.URLAPI}/${path}`, data, { headers: header});
  }

  makeDeleteRequest(path: string): Observable<any> {
    return this.http.delete(`${this.URLAPI}/${path}`);
  }
}
