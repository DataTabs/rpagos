
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from './global-api.service';
import { AuthService } from 'app/core/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ControlTables {


  Estructura: any = {
    "nodeForeignKey": {

    },
    "rpClientes":   [{
                        "Field": "idCliente",	
                        "Type": "varchar",
                        "length": 120,
                        "Null": false,
                        "Key": true,
                        "Default": "",
                        "Extra": "" 
                    },
                    {
                        "Field": "idCliente",	
                        "Type": "varchar",
                        "length": 120,
                        "Null": false,
                        "Key": true,
                        "Default": "",
                        "Extra": "" 
                    }
    ],
        
    "rpClientesExtraTipoDatos": {

    },
    "rpClientesExtras": {},
    "rpClientesStatus": {},
    "rpControlCobranza": {},
    "rpControlPagos": {},
    "rpCuenta": {},
    "rpEmpresas": {},
    "rpEmpresasBancos": {},
    "rpEmpresas_FormaPago": {},
    "rpFacturaEstados": {},
    "rpFacturaTaxDescuentos": {},
    "rpFacturas": {},
    "rpFacturas_Detalles": {},
    "rpFormarPago": {},
    "rpLogControl": {},
    "rpPaises": {},
    "rpPermisoTipo": {},
    "rpPermisos": {},
    "rpRoles": {},
    "rpRolesPermisos": {},
    "rpServicios": {},
    "rpTemplateTipo": {},
    "rpTemplates": {},
    "rpTipoClientes": {},
    "rpUserEmpresa": {},
    "rpUsers": {},
    "rpVendedores": {},
    "viewClientes": {},
    "viewControlCobranza": {},
  }
  constructor(private globalServiceApi: GlobalApiService, private _AuthService: AuthService) { }


  getColumByTable(table: string): any {
    return this.Estructura[table];
  }



}


