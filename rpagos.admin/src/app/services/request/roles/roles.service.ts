import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from '../global-api.service';
import { DataRelationsCreations, DataRelationsUpdate }  from '../../../mock-api/relations/dataBaseRelations';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  private table: string = 'rpRoles'; 
  private relations;
  constructor( private globalApi: GlobalApiService ) { }

  getRolesByEmpresa(IdRol:any): Observable<any> {
    return this.globalApi.makeGetRequest(`find-any-info/${this.table}/idEmpresa/${IdRol}`);
  }

  createRoles(roles: any): Observable<any> {
    console.log('')
    this.relations = DataRelationsCreations.find((relation) => relation.table === this.table);
    if( this.relations === undefined ) {
      this.relations.keysToAddId = []
      this.relations.arrayIntoObj = []
      this.relations.relations = 'false';
    }

    return this.globalApi.makePostRequest(`create-info/${this.table}`, roles, {
      'x-keys-to-add-id': ''+this.relations.keysToAddId+'',
      'x-keys-of-arrays': ''+this.relations.arrayIntoObj+'',
      'x-relations': this.relations.relations
    });
  }

}
