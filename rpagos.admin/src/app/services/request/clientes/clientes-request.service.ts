import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from '../global-api.service';
import { DataRelationsCreations, DataRelationsUpdate } from '../../../mock-api/relations/dataBaseRelations';
@Injectable({
  providedIn: 'root'
})
export class ClientesRequestService {
  table="rpClientes"
  header

  constructor( private globalServiceApi: GlobalApiService ) {
      this.header = DataRelationsCreations.find((item: any)=>item.table == this.table)
  }


  // PETICIONES GET
  /**
 * Returns all clients by idEmpresa
 * @param {id} id de la empresa
 * @returns {clientes} array of clients
 */
//find-any-info/rpClientes/idEmpresa/1
  getClientesAll(id:any): Observable<any>{
  return this.globalServiceApi.makeGetRequest(`find-any-info/${this.table}/idEmpresa/${id}`)
  }

  getClienteByPk(pk: string): Observable<any>  {
    return this.globalServiceApi.makeGetRequest('find-cliente-by-pk/'+pk);
  }

  getClienteByCode(code: string): Observable<any> {
    return this.globalServiceApi.makeGetRequest('find-cliente-by-code/'+code);
  }

  getClienteByName(name: string): Observable<any>  {
    return this.globalServiceApi.makeGetRequest('find-cliente-by-name/'+name);
  }


  getAllTipoDatoExtra(): Observable<any>  {
    return this.globalServiceApi.getData('rpClientesExtraTipoDatos','status','1');
  }


  getDatosClientesExtras(id: string): Observable<any> {
    return this.globalServiceApi.getData('rpClientesExtras', 'idCliente', id);
  }


  // PETICONES POST

  addCliente(cliente: any): Observable<any> {
      console.log("addCliente: ",cliente)
    return this.globalServiceApi.makePostRequest(`create-info/${this.table}`,cliente,
    {
       "Content-Type": "application/json",
       "x-keys-to-add-id": '["idCliente",  "idExtraCliente"]',
       "x-keys-of-arrays": '["rpClientesExtras"]',
       "x-relations":"true"
    } )
  }

  // PETICONES PUT

  updateCliente(updated: any): Observable<any> {
    return this.globalServiceApi.makePutRequest(`update-info/${this.table}/idCliente`,updated,
    {
        "x-multiple-update": "true",
        "x-elements-obj": '["rpClientesExtras"]',
        "x-attr-duplicate": '[[ "TipoDato", "Descrip", "Status" ]]',
    }
    )
  }


  // PETICONES DELETE

  deleteInfoCliente(id: string) {
    return this.globalServiceApi.makeDeleteRequest('delete-cliente-and-extra/'+id);
  }

}

export const test=
{
    "idCliente": "",
	"idEmpresa": 0,
	"CodeCliente":	"019",
	"idTipoClientes": 1,
	"ClienteName":	"Carlos",
	"avatar": "https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png",
	"rif_nit": "J-6565211",
	"idPais":1,
	"idCiudad": 1,
	"StatusC": 1,
	"nota" : "esta es una nota de la creacion del cliente",
    "rpClientesExtras": [
        {
            "idExtraCliente": "",
            "idCliente": "",
            "TipoDato": 1,
            "Descrip": "una descripcion del extra",
            "Status": 4
        }
    ]
}
