import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from '../global-api.service';
import { DataRelationsCreations, DataRelationsUpdate } from '../../../mock-api/relations/dataBaseRelations';
import { AuthService } from 'app/core/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class FacturaListService {

  private table = "rpFacturas"
  private relations;

  constructor(
    private globalServiceApi: GlobalApiService,
    private _AuthService: AuthService
  ) { }

  getFacturasByEmpresa(): Observable<any> {
    const { idCuenta } = this._AuthService.getDecodedAccessToken()
    return this.globalServiceApi.makeGetRequest(`find-any-info/${this.table}/idEmpresa/12`);
  }
  

  getServicesByFactura(listServices: string[]) {
    return this.globalServiceApi.makeGetRequest(`find-in/rpServicios/idServicios`, {
      'x-data-values': `"${listServices}"`
    });
  }

  getServicesByEmpresa(idEmpresa: string) {
    return this.globalServiceApi.makeGetRequest(`find-any-info/rpServicios/idEmpresa/${idEmpresa}`);
  }

}
