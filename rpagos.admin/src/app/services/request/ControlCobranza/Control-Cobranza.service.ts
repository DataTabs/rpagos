import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from '../global-api.service';
import { DataRelationsCreations, DataRelationsUpdate } from '../../../mock-api/relations/dataBaseRelations';
import { AuthService } from 'app/core/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ControlCobranzaService {

  private table = "rpControlCobranza"
  private relations;

  constructor(
    private globalServiceApi: GlobalApiService,
    private _AuthService: AuthService
  ) { }

  getControlCobranzaByFactura(Id:any): Observable<any> {
    const { idCuenta } = this._AuthService.getDecodedAccessToken()
    return this.globalServiceApi.makeGetRequest(`find-any-info/${this.table}/idFactura/${Id}`);
  }
  
  updateControlCobranza(dataForm: any) {
    return this.globalServiceApi.makePutRequest(`update-info/${this.table}/idControlCobranza`, dataForm, {
      'x-attr-duplicate': '[""]',
      'x-elements-obj': '[""]',
      'x-multiple-update': 'false'
    })
  }


}
