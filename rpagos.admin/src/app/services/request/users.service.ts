import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from './global-api.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
    table="rpUser"

  constructor( private globalServiceApi: GlobalApiService ) {
  }

  createUser1Time(user:any): Observable<any>{
    console.log(user) 
  return this.globalServiceApi.makePostRequest('register-user',user)
  }


  public authUser(user:any): Observable<any> {
    return this.globalServiceApi.makePostRequest('login-user',{
        Email: user.email,
        PassUser: user.password
    })
  }


}
