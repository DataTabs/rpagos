import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from '../global-api.service';
import { DataRelationsCreations, DataRelationsUpdate } from '../../../mock-api/relations/dataBaseRelations';
import { AuthService } from 'app/core/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserListService {

  private table = "rpUsers"
  private relations;

  constructor(
    private globalServiceApi: GlobalApiService,
    private _AuthService: AuthService
  ) { }

  getAllUserByidCuenta(): Observable<any> {
    const { idCuenta } = this._AuthService.getDecodedAccessToken()
    return this.globalServiceApi.makeGetRequest(`find-any-info/${this.table}/idCuenta/${idCuenta}`);
  }

  createUser(userData: any) {
    this.relations = DataRelationsCreations.find((relation) => relation.table === this.table);
    if( this.relations === undefined ) {
      this.relations = {};
      this.relations.keysToAddId = '["idUsers"]'
      this.relations.arrayIntoObj = '[]'
      this.relations.relations = 'false';
    }

    return this.globalServiceApi.makePostRequest(`create-info/${this.table}`, userData, {
      'x-keys-to-add-id': ''+this.relations.keysToAddId+'',
      'x-keys-of-arrays': ''+this.relations.arrayIntoObj+'',
      'x-relations': this.relations.relations
    });
  }

  verifyIfUserExist(email: string): Observable<any> {
    return this.globalServiceApi.makeGetRequest(`find-any-info/${this.table}/Email/${email}`);
  }

  changePassword(oldPass: string, newPass: string, email: string) {
    return this.globalServiceApi.makeGetRequest(`change-pass-user/${oldPass}/${newPass}/${email}`);
  }

  updateInfoUser( data: any ) {
    return this.globalServiceApi.makePutRequest(`update-info/${this.table}/idUsers`, data, {
      'x-attr-duplicate': '[""]',
      'x-elements-obj': '[""]',
      'x-multiple-update': 'false'
    });
  }

}
