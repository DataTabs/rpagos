import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from './global-api.service';
import { AuthService } from 'app/core/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class FindOneAnyService {

  constructor(private globalServiceApi: GlobalApiService, private _AuthService: AuthService) { }

  getData(Table: string, Campo: string, Dato: any): Observable<any> {
    const { idCuenta } = this._AuthService.getDecodedAccessToken()
    return this.globalServiceApi.makeGetRequest(`find-any-info/${Table}/${Campo}/${Dato}`);
  }

}
