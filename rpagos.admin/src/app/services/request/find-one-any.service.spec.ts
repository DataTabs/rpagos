import { TestBed } from '@angular/core/testing';

import { FindOneAnyService } from './find-one-any.service';

describe('FindOneAnyService', () => {
  let service: FindOneAnyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FindOneAnyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
