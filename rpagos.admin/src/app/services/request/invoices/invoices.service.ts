import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalApiService } from '../global-api.service';
import { Factura } from '../../../modules/admin/pages/invoice/printable/factura.type';
import { DataRelationsCreations, DataRelationsUpdate }  from '../../../mock-api/relations/dataBaseRelations';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  private table: string = 'rpFacturas';
  private relations;
  constructor( private globalServiceApi: GlobalApiService ) { }

  getAllInvoicesByIdEmpresa(idEmpresa: string): Observable<Factura[]> {
    return this.globalServiceApi.makeGetRequest(`find-any-info/${this.table}/idEmpresa/${idEmpresa}`);
  }

  createFactura(factura: any) {
    this.relations = DataRelationsCreations.find((relation) => relation.table === this.table);
    if ( this.relations === undefined ) {
      this.relations.keysToAddId = []
      this.relations.arrayIntoObj = []
      this.relations.relations = 'false';
    }
    return this.globalServiceApi.makePostRequest(`create-info/${this.table}`, factura , {
      'x-keys-to-add-id': ''+this.relations.keysToAddId+'',
      'x-keys-of-arrays': ''+this.relations.arrayIntoObj+'',
      'x-relations': this.relations.relations
    });
  }

}
