/* eslint-disable */
export const user = {
    id     : 'cfaad35d-07a3-4447-a6c3-d8c3d54fd5df',
    name   : 'Enrique Cheang',
    email  : 'E.Cheang@company.com',
    company: 'ECV & ASOCIADOS',
    role   : 'Master',
    avatar : 'assets/images/avatars/brian-hughes.jpg',
    status : 'online'
};
