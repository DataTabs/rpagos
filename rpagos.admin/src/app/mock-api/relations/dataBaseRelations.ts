export const DataRelationsCreations = [
    {
        table: 'rpClientes',
        keysToAddId: '["idCliente",  "idExtraCliente"]',
        arrayIntoObj: ["rpClientesExtras"],
        relations: "true"
    },
    {
        table: 'rpFacturas',
        keysToAddId: '["idFacturas",  "idFacturaEstados", "idFacturas_Detalle", "idFacturaTaxDescuentos"]',
        arrayIntoObj: '["rpFacturaEstados", "rpFacturas_Detalles", "rpFacturaTaxDescuentos"]',
        relations: "true"
    },
    {
        table: 'rpRoles',
        keysToAddId: '["idRolU", "idRolPerU"]',
        arrayIntoObj: '["rpRolesPermisos"]',
        relations: "true"
    },
    {
        table: 'any',
        keysToAddId: [],
        arrayIntoObj: [],
        relations: "false"
    }
]

export const DataRelationsUpdate = [
    {
        table: 'rpClientes',
        elementsObj: '["rpClientesExtras"]',
        attrDuplicate: '[[ "TipoDato", "Descrip", "Status" ]]',
        multipleUpdate: "true"
    },
    {
        table: 'rpFacturas',
        elementsObj: '["rpFacturaEstados", "rpFacturas_Detalles", "rpFacturaTaxDescuentos"]',
        attrDuplicate: '[["Descripcion"], ["idServicio", "Descripcion", "Cantidad", "Monto"], ["tipo", "Monto"]]',
        multipleUpdate: "true"
    }
]