import { Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree, Router, Data } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthService } from 'app/core/auth/auth.service';
import { InitialData } from 'app/app.types';
import { user } from 'app/mock-api/common/user/data';


@Injectable({
  providedIn: 'root'
})
export class RolesGuard implements CanActivate, CanActivateChild {
  data: InitialData;
  access = user.role;
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  constructor(private _router: Router, private _authService: AuthService, private _activatedRoute: ActivatedRoute) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const redirectUrl = state.url === '/pages/error/401' ? '/' : state.url;
    return this._check(redirectUrl);
  }
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

  private _check(redirectURL: string): Observable<boolean> {
    // Revisa los permisos del usuario
    if (user.role === 'Admin' || user.role === 'User') {
      // Redirige al usuario al componente de "Acceso denegado"    
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
      console.log(user.role);
      console.clear();
      this._router.navigate(['/sign-out']);
      // Niega el acceso devolviendo falso
      return of(false);
      //return of(true);
    }
    console.log(user.role)
    // Concede el acceso devolviendo verdadero
    return of(true);


  }

}
