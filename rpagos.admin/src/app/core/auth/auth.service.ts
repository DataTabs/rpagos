import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { UserService } from 'app/core/user/user.service';
import { UsersService } from 'app/services/request/users.service';
import jwt_decode from 'jwt-decode';
import { AuthMockApi } from 'app/mock-api/common/auth/api';

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
 

@Injectable()
export class AuthService
{
    private _authenticated: boolean = false;
    public _DataLogin: any = {};

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService, 
        public _UsersService: UsersService, 
        public _AuthMockApi: AuthMockApi
    ){
        const ValueToken = this.getDecodedAccessToken();
        this._DataLogin = ValueToken;
    } 

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for access token
     */
    set accessToken(token: string)
    {
        console.log("PASO accessToken")
        localStorage.setItem('accessToken', token);
    }

    get accessToken(): string
    {
        return localStorage.getItem('accessToken') ?? '';
    }


    set accessTokenBackEnd(token: string)
    {
        console.log("PASO accessToken")
        localStorage.setItem('token', token);
    }

    get accessTokenBackEnd(): string
    {
        return localStorage.getItem('token') ?? '';
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Forgot password
     *
     * @param email
     */
    forgotPassword(email: string): Observable<any>
    {
        return this._httpClient.post('api/auth/forgot-password', email);
    }

    /**
     * Reset password
     *
     * @param password
     */
    resetPassword(password: string): Observable<any>
    {
        return this._httpClient.post('api/auth/reset-password', password);
    }

    /**
     * Sign in
     *
     * @param credentials
     */
    signIn(credentials: { email: string; password: string }): Observable<any>
    {
        // Throw error, if the user is already logged in
        if ( this._authenticated )
        {
            return throwError('User is already logged in.');
        }

        //!TODO Quitar el inmemory :)
        const User = {
            email: credentials.email,
            password: credentials.password
        }
        console.log(User)
        
        /*return this._httpClient.post('api/auth/sign-in',
        {
            email:'E.Cheang@company.com',
            password:'admin',
        })*/
        // Codigo de JHONATTAN NO CAMBIA NI BORRAR!!!!!!!
        return this._UsersService.authUser(User)
        .pipe(
            switchMap((response: any) => {
                console.log("response: ", response)


                this.accessTokenBackEnd = response.token
                const ValueToken = this.getDecodedAccessToken()

                this._DataLogin = ValueToken
                console.log(ValueToken);
                 
                const SalidaUser = {
                    accessToken: this._AuthMockApi._generateJWTToken(),
                    accessTokenBackend: response.token,                    
                    tokenType: "bearer",
                    user: {
                        avatar: "assets/images/avatars/brian-hughes.jpg",
                        company: this.getEmpresaName(ValueToken.idCuenta),
                        email: ValueToken.Email,
                        id: "cfaad35d-07a3-4447-a6c3-d8c3d54fd5df",
                        name: ValueToken.NameUser,
                        status: "online",
                        role: 'Master'
                    }
                }

                /*
                Lo que debe contrner response
                accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzgwNTkwMTAsImlzcyI6IkZ1c2UiLCJleHAiOjE2Mzg2NjM4MTB9._XiVj_t3bxHp4-_T7BffFDKbinr0aMa-T1SP2G-C7Co"
                tokenType: "bearer"
                user:
                avatar: "assets/images/avatars/brian-hughes.jpg"
                company: "ECV & ASOCIADOS"
                email: "E.Cheang@company.com"
                id: "cfaad35d-07a3-4447-a6c3-d8c3d54fd5df"
                name: "Enrique Cheang"
                status: "online"
                */

                // Store the access token in the local storage
                this.accessToken =  SalidaUser.accessToken;
                

                // Set the authenticated flag to true
                this._authenticated = true;

                // Store the user on the user service
                this._userService.user = SalidaUser.user;


               // console.log(this.getDecodedAccessToken('aquiValaLlavePrivada'))

                // Return a new observable with the response
                return of(SalidaUser);
            })
        );
    }


    getEmpresaName(idCuenta) {
        // Con el idCuenta consulto la BD
        return "ECV & ASOCIADOS"
    }
    getDecodedAccessToken(): any {
        try{

            return jwt_decode(this.accessTokenBackEnd);

            /*
                Email: "29dalfonzo@gmail.com"
                LoginUser: "29dalfonzo@gmail.com"
                Movil: null
                NameUser: "Daniel Alfonzo"
                PassUser: "$2b$10$ZG3w9di2m9pfcqHvOIBCYOsDREweHKD8aBDY/CuZQI4lRrv4yUhBe"
                Status: 0
                createdAt: "2021-11-20T20:42:40.000Z"
                exp: 1638075325
                iat: 1638068125
                idCuenta: 26
                idRol: 0
                idUsers: 19
                updatedAt: "2021-11-20T20:42:40.000Z"
            */
        }
        catch(Error){
            return null;
        }
      }

    /**
     * Sign in using the access token
     */
    signInUsingToken(): Observable<any>
    {
        // Renew token
        console.log("Pase!" + this.accessToken)
        return this._httpClient.post('api/auth/refresh-access-token', {
            accessToken: this.accessToken
        }).pipe(
            catchError(() =>

                // Return false
                of(false)
            ),
            switchMap((response: any) => {

                // Store the access token in the local storage
                this.accessToken = response.accessToken;

                // Set the authenticated flag to true
                this._authenticated = true;

                // Store the user on the user service
                this._userService.user = response.user;

                // Return true
                return of(true);
            })
        );
    }

    /**
     * Sign out
     */
    signOut(): Observable<any>
    {
        // Remove the access token from the local storage
        localStorage.removeItem('accessToken');

        // Set the authenticated flag to false
        this._authenticated = false;

        // Return the observable
        return of(true);
    }

    /**
     * Sign up
     *
     * @param user
     */
    signUp(user: { name: string; email: string; password: string; company: string }): Observable<any>
    {
        return this._httpClient.post('api/auth/sign-up', user);
    }

     /**
     * Sign up
     *
     * @param Empresa Recibe empresa a registrar
     */
    RegistrarEmpresa(Empresa:any): Observable<any>
    {
        console.log('me llego esta empresa: ', Empresa)
        return
    }

    /**
     * Unlock session
     *
     * @param credentials
     */
    unlockSession(credentials: { email: string; password: string }): Observable<any>
    {
        return this._httpClient.post('api/auth/unlock-session', credentials);
    }

    /**
     * Check the authentication status
     */
    check(): Observable<boolean>
    {
        // Check if the user is logged in
        if ( this._authenticated )
        {
            return of(true);
        }

        // Check the access token availability
        if ( !this.accessToken )
        {
            return of(false);
        }

        // Check the access token expire date
        if ( AuthUtils.isTokenExpired(this.accessToken) )
        {
            return of(false);
        }

        // If the access token exists and it didn't expire, sign in using it
        return this.signInUsingToken();
    }
}
