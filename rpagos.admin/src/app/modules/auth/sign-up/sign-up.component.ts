import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators, FormControl, FormArray } from '@angular/forms';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { countries } from '../../../../rpagos-db/countries-data';
import { Client } from '../../../../models/client';
import { BuildModelsService } from 'app/services/BuildModels/build-models.service';
import { UsersService } from 'app/services/request/users.service';

@Component({
    selector     : 'auth-sign-up',
    templateUrl  : './sign-up.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
    providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
    styleUrls: ['./sign-up.component.scss']
})
export class AuthSignUpComponent implements OnInit
{

    public countries:any = countries;

    @ViewChild('signUpNgForm') signUpNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signUpForm: FormGroup;
    showAlert: boolean = false;
    checked: boolean = false;
    location: Location;
    client: Client;
    hide = true;

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        location: Location,
        private _BuildModelsService:BuildModelsService,
        private _UsersService:UsersService
    ){
        this.location = location;
     }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signUpForm = this._formBuilder.group({
                name        : ['', Validators.required],
                email       : ['', [Validators.required, Validators.email]],
                password    : ['', Validators.required],
                agreements  : ['', Validators.requiredTrue],
                phoneNumber : ['', Validators.required],
                reasonSocial: ['', Validators.required],
                nameCompany : ['', Validators.required],
                country     : ['', Validators.required]
            }
        );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    //Return path of url
    getPath() {
        return console.log(this.location.path());
    };

    /**
     * Sign up
     */
    signUp(): void
    {
        if ( this.signUpForm.invalid )
        {
            return console.log("Form invalid");
        }


        // Hide the alert
        this.showAlert = false;
        //let builded = this._BuildModelsService.BuildEmpresatoDB(this.signUpForm.value)
        let Userbuilded = this._BuildModelsService.BuildUser(this.signUpForm.value)
        this._UsersService.createUser1Time(Userbuilded).subscribe((res) => {
            console.log(res)
            if (!res) return
        // Sign up
        this._authService.signUp(this.signUpForm.value)
            .subscribe(
                (response) => {

                    // Navigate to the confirmation required page
                    this._router.navigateByUrl('/confirmation-required');
                },
                (response) => {

                    // Re-enable the form
                    this.signUpForm.enable();

                    // Reset the form
                    this.signUpNgForm.resetForm();

                    // Set the alert
                    this.alert = {
                        type   : 'error',
                        message: 'Something went wrong, please try again.'
                    };

                    // Show the alert
                    this.showAlert = true;
                }
            );
        })


    }
}
