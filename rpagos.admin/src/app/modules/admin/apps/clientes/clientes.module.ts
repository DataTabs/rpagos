import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesComponent } from './clientes.component';
import { RouterModule } from '@angular/router';
import { ClientesRoutes } from './clientes.routing';
import { MasterListAddModule } from 'app/modules/admin/apps/Master/Report/MasterListAdd/MasterListAdd/MasterListAdd.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { ClientesExtraModule } from './clientes-extra/clientes-extra.module';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatIconModule,
    ClientesExtraModule,
    MasterListAddModule,
    RouterModule.forChild(ClientesRoutes),    
    CommonModule
  ],
  declarations: [ClientesComponent]
})
export class ClientesModule { }



