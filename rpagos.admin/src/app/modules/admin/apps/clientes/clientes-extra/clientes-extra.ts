export interface iClientesExtra {
    idExtraCliente: number;
    idCliente: number;
    TipoDato: number;
    Descrip: string;
    Etiqueta: string;
    Status: number;
    createdAt: string;
    updatedAt: string;

}


export interface iTipoDatosCE {  // Tipo de Datos Cliente Extra
    idTipo: number;    
    Descrip: string;
    i18n: string;    
    icon: string;

}



export interface ExampleTab {
    label: string;
    content: string;
  }