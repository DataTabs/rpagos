import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'app/core/auth/auth.service';
import { ClientesRequestService } from 'app/services/request/clientes/clientes-request.service';
import { iTipoDatosCE, iClientesExtra, ExampleTab } from './clientes-extra';
import {Observable, Observer} from 'rxjs';


@Component({
  selector: 'app-clientes-extra',
  templateUrl: './clientes-extra.component.html',
  styleUrls: ['./clientes-extra.component.scss']
})
export class ClientesExtraComponent implements OnInit {

  contactForm: FormGroup;
  TipoDatoClienteExtra:iTipoDatosCE[] = [];
  DatoClienteExtra:iClientesExtra[] = [];
  asyncTabs: ExampleTab[] = [];

  @Input() idCliente: string;


  constructor( private _authService: AuthService, 
    private _formBuilder: FormBuilder,
              private _changeDetectorRef: ChangeDetectorRef,
              private _ClientesRequestService:ClientesRequestService) { 

                console.log(this._authService._DataLogin.idEmpresaActiva);
                
   
  }

  ngOnInit() {

    console.log(this.idCliente);
    // Tengo que buscar todos los registro que tengo en ClientesExtras con this.idCliente
    this._ClientesRequestService.getDatosClientesExtras(this.idCliente).subscribe((Data: any)  => { 
      
      this.DatoClienteExtra = Data;
      console.log(this.TipoDatoClienteExtra);
    });

    this._ClientesRequestService.getAllTipoDatoExtra().subscribe((res) => {
                console.log(res);
                let group={}   
                var _TipoDatoClienteExtra:iTipoDatosCE[] = [];

                
                res.forEach(element => {
                        const JsonPush = {
                        idTipo: element.idClientesExtraTipoDatos,
                        Descrip: element.Descripcion,
                        i18n: element.i18n, 
                        icon: "heroicons_solid:"+element.iconShow }
                      _TipoDatoClienteExtra.push(JsonPush);

                      this.asyncTabs.push({label: "heroicons_solid:"+element.iconShow, content: 'Content 1'})
                      group[element.i18n]= this._formBuilder.array([])            
                    
                });
                this.TipoDatoClienteExtra = _TipoDatoClienteExtra;        
            

              this.contactForm = this._formBuilder.group(group);
              console.log(this.contactForm);

              const emailFormGroups = [];

                  emailFormGroups.push(this._formBuilder.group({
                      email: ['Hola'],
                      label: [''],
                      idExtraCliente:['']
                  }));

                  emailFormGroups.push(this._formBuilder.group({
                    email: ['Hola'],
                    label: [''],
                    idExtraCliente:['']
                }));



              // Add the email form groups to the emails form array
              this.TipoDatoClienteExtra.forEach(element => {
                      // Filtro los Datos segun el Tipo y recorro el resultado
                      const DatainDB = this.DatoClienteExtra.filter(Value => Value.TipoDato == element.idTipo);
                      console.log(DatainDB);
                
                      if (DatainDB.length > 0) { 
                            DatainDB.forEach((ElementGroup) => {
                              (this.contactForm.get(element.i18n) as FormArray).push( this._formBuilder.group({
                                Descrip: [ElementGroup.Descrip],
                                label: [ElementGroup.Etiqueta],
                                idExtraCliente:[ElementGroup.idExtraCliente]
                            }));
                          });
                      } 
                      else { 
                        // Agrego 1 elemento Base
                        (this.contactForm.get(element.i18n) as FormArray).push( this._formBuilder.group({
                          Descrip: [''],
                          label: [''],
                          idExtraCliente:['']
                          }));
                      }
                      
              });
              console.log(this.contactForm);
              
    })

  }

  removeField(index: number, Campo): void
    {
        // Get form array for emails
        const emailsFormArray = this.contactForm.get(Campo) as FormArray;

        // Remove the email field
        emailsFormArray.removeAt(index);

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

  addField(Campo): void
  {
      // Create an empty email form group
      const emailFormGroup = this._formBuilder.group({
          Descrip: [''],
          label: [''],
          idExtraCliente: ['']
      });

      // Add the email form group to the emails form array
      (this.contactForm.get(Campo) as FormArray).push(emailFormGroup);

      // Mark for check
      this._changeDetectorRef.markForCheck();
  }

  /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
   trackByFn(index: number, item: any): any
   {
       return item.id || index;
   }

 

}
