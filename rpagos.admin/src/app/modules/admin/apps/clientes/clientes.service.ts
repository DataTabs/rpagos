import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from 'app/core/auth/auth.service';
import { EMPTY  } from "rxjs";
import { ClientesRequestService } from 'app/services/request/clientes/clientes-request.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

constructor(private _httpClient: HttpClient,
  private _authService: AuthService,
  private _ClientesRequestService:ClientesRequestService,) { }

    getClientes():Observable<any>{
        console.log(this._authService._DataLogin.idEmpresaActiva);
        
          if (this._authService._DataLogin.idEmpresaActiva != null && this._authService._DataLogin.idEmpresaActiva != undefined) {
            return this._ClientesRequestService.getClientesAll(this._authService._DataLogin.idEmpresaActiva); 
          } else return EMPTY;
    }

}
