import { Component, OnInit } from '@angular/core';
import { DatosListGeneric } from '../Master/DatosListGeneric';
import { ClientesService } from './clientes.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {

  showTable: boolean = false

  setForm: any = []
  dataTable: any = []

  public Parametros: DatosListGeneric = {
    Title: "Clientes",          // Titulo para mostrar
    TableName: "rpClientes",      // Nombre de la Tabla en Base de Datos / Inmemory
    Menu: true,                 // Indica si la lista tiene Menú
    MultiSelect: false,         // Seleccion multiple en la lista
    ArgMenu: ["Editar/edit", "Eliminar/delete"], //  // Menú de Opciones en la lista
    DownloasExcel: true,
    ConfigCampos: [
      {
        TypeDato: 'string',
        KeyJSON: 'rif_nit',
        HeadShowList: 'Rif/Nit',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Nombre es Requerido',
        flex: 'flex: 0 0 15%;'
      },
      {
        TypeDato: 'string',
        KeyJSON: 'ClienteName',
        HeadShowList: 'Nombre Cliente',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Agente es Requerido',        
        flex: 'flex: 0 0 70%;'
      } 
    ]
  }


  constructor(_ClientesService: ClientesService) { 

    _ClientesService.getClientes().subscribe((Data: any)  => {
      console.log(Data);
      this.dataTable = Data;
      this.showTable = true;
    });
  }

  ngOnInit() {
  }

}
