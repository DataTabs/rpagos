import { Component, OnInit } from '@angular/core';
import { FinfAnyService } from 'app/services/finf-any.service';
import { EmpresaListService } from 'app/services/request/empresa-list/empresa-list.service';
import { DatosListGeneric } from '../../Master/DatosListGeneric';
import {ServiciosService } from 'app/services/request/servicios/servicios.service'

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.scss']
})
export class ServiciosComponent implements OnInit {

  idEmpresa:any 
  showTable: boolean = false;
  dataTable: any[];

  constructor(private _FinfAnyService:FinfAnyService,
              private empresalistservice: EmpresaListService,
              private _servicios: ServiciosService) 
  { 
    this.GetEmpresa();
  }

  public Parametros: DatosListGeneric = {
    Title: "Servicios",          // Titulo para mostrar
    TableName: "rpServicios",      // Nombre de la Tabla en Base de Datos / Inmemory
    Menu: true,                 // Indica si la lista tiene Menú
    MultiSelect: false,         // Seleccion multiple en la lista
    ArgMenu: ["Editar/edit", "Eliminar/delete"], //  // Menú de Opciones en la lista
    DownloasExcel: true,
    ConfigCampos: [
      /*TypeDato: 'avatar',     // Tipo de Datos para la Lista {string, phone, date, icon, avatar, img}
      KeyJSON: 'Avater',      // El canpo que viene en el json
      HeadShowList: 'Avatar', // Title de la lista
      TypeForm: 'avatar',     // Tipo de Dato para el Formulario
      Required: false,        // Si el campo es requerido para el Form
      showlist: false,        // Si se va a mostrar en la lista
      IndexOrderList: 0,      // En que orden va salir en la lista
      IndexOrderForm: 0,      // En que orden va salir en el Formulario
      ErrorShowForm: ''  */     // Texto en caso de Error
      
      {
        TypeDato: 'string',
        KeyJSON: 'idServicios',
        HeadShowList: 'idServicio',
        TypeForm: 'input',
        Required: true,
        showlist: false,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'idServicio es Requerido', 
        flex: 'flex: 0 0 15%;'
      }, 
      {
        TypeDato: 'string',
        KeyJSON: 'DescripTitulo',
        HeadShowList: 'Título',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Título es Requerido',         
        flex: 'flex: 0 0 15%;'
       },
       {
        TypeDato: 'string',
        KeyJSON: 'DescripBody',
        HeadShowList: 'Descripción',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Descripción es Requerido', 
        flex: 'flex: 0 0 15%;'
       },
       {
        TypeDato: 'phone',
        KeyJSON: 'Monto1',
        HeadShowList: 'Primer monto',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Monto uno es Requerido', 
        flex: 'flex: 0 0 15%;'
       },
       {
        TypeDato: 'phone',
        KeyJSON: 'Monto2',
        HeadShowList: 'Segundo Monto',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Monto dos es Requerido', 
        flex: 'flex: 0 0 15%;'
       },
      
    ]
  }

  ngOnInit(): void {
  }

  DataSalida(event) {
    console.log(event);
    switch (event.Option) {
      case 0:
        console.log("editar");
        this.UpdateServicio(event.Data);
        // Opcion 0 es para la edicion de un servicio
        break;
      case 1:
        console.log("eliminarrrrrrrrr");
        this.DeleteServicio(event.Data);
        // Opcion 1 para la eliminacion de un servicio
        break;

      default:
        console.log("creacion");
        console.log(event.Option);
        this.CreateServicio(event.Data);
        // Por defecto la creacion de un servicio
        break;
    }
  }

  FindDBServicios(){
    this._FinfAnyService.table = "rpServicios";
    this._FinfAnyService.Campo = "idEmpresa";
    this._FinfAnyService.Valor = this.idEmpresa;
    try {
          this._FinfAnyService.getAllData().subscribe((res) => {         
            console.log("Servicios");
            console.log(res);
            console.log(this.idEmpresa);
            if (Array.isArray(res) && res.length > 0) {
                  this.dataTable = res;
                  this.showTable = true;
                  console.log(this.showTable);
                  console.log(this.dataTable);
            }

            });
      } catch (error) {
            console.error(error)
    } 
  }

  GetEmpresa() {
    try {
      //No hay promesa porque el GetIdEmpresaActiva es un return;
      this.idEmpresa =this.empresalistservice.GetIdEmpresaActiva();
      this.FindDBServicios();
    } catch (error) {
      console.error(error)
    }
  }

  CreateServicio(Data:any){
    try {
      let JSONServicio =[{
        idEmpresa: this.idEmpresa,
        Estado:"1", //Por defecto por los momentos
        FechaCreado:new Date()
      }];
  
      Data.forEach(element => {
        JSONServicio[0][element.ID]=element.Value
      });
  
      this._servicios.addServicio(JSONServicio[0]).subscribe((res) => {
        console.log(res);
      });
    } catch (error) {
      console.error(error);
      console.log(new Date());
    }
   
  }

  UpdateServicio(Data:any){
    try {
      console.log("UpdateServicio");
      let JSONServicio =[{
        idEmpresa: this.idEmpresa
      }];
      Data.forEach(element => {
        JSONServicio[0][element.ID]=element.Value
      });
  
      this._servicios.updateServicio(JSONServicio[0]).subscribe((res) => {
        console.log(res);
      });
    } catch (error) {
      console.error(error);
      console.log(new Date());
    }
   
  }

  DeleteServicio(Data:any){
    console.log("DeleteServicio");
    let UpdateCDelete=Data.find((service)=>service.ID=="idServicios");
    console.log("UpdateCDelete");
    console.log(UpdateCDelete);
  }


}
