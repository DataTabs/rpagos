import { Component, OnInit} from '@angular/core';
import { DatosListGeneric } from '../Master/DatosListGeneric';
import { RolesService }  from '../../../../services/request/roles/roles.service';
import { FinfAnyService } from 'app/services/finf-any.service';
import { UserListService } from '../../../../services/request/user-list/user-list.service';
import { EmpresaListService } from 'app/services/request/empresa-list/empresa-list.service';
import AllDaySplitter from '@fullcalendar/timegrid/AllDaySplitter';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']  
})
export class RolesComponent implements OnInit {

  public showTable: boolean = false
  setForm: any = [];

  public Permisos:any=[]
  private PermisosToForm:String="";
  private Users:any=[]
  private ListaUserForm:String="";
  public dataTable2: any=[]

  public Parametros: DatosListGeneric = {
    Title: "Roles",          // Titulo para mostrar
    TableName: "rpRoles",      // Nombre de la Tabla en Base de Datos / Inmemory
    Menu: true,                 // Indica si la lista tiene Menú
    MultiSelect: false,         // Seleccion multiple en la lista
    ArgMenu: ["Editar/edit", "Eliminar/delete"], //  // Menú de Opciones en la lista
    DownloasExcel: true,
    ConfigCampos: [
      /*TypeDato: 'avatar',     // Tipo de Datos para la Lista {string, phone, date, icon, avatar, img}
      KeyJSON: 'Avater',      // El canpo que viene en el json
      HeadShowList: 'Avatar', // Title de la lista
      TypeForm: 'avatar',     // Tipo de Dato para el Formulario
      Required: false,        // Si el campo es requerido para el Form
      showlist: false,        // Si se va a mostrar en la lista
      IndexOrderList: 0,      // En que orden va salir en la lista
      IndexOrderForm: 0,      // En que orden va salir en el Formulario
      ErrorShowForm: ''  */     // Texto en caso de Error
      
       {
        TypeDato: 'string',
        KeyJSON: 'Usuarios',
        HeadShowList: 'Usuarios',
        TypeForm: 'select',
        Required: true,
        showlist: false,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Usuarios es Requerido', 
        flex: 'flex: 0 0 15%;'
       },
       {
        TypeDato: 'string',
        KeyJSON: 'idRol',
        HeadShowList: 'idRol',
        TypeForm: 'input',
        Required: true,
        showlist: false,
        IndexOrderList: 1,
        IndexOrderForm: 1,
        ErrorShowForm: 'idRol es Requerido', 
        flex: 'flex: 0 0 15%;'
      },
       {
        TypeDato: 'string',
        KeyJSON: 'idEmpresa',
        HeadShowList: 'idEmpresa',
        TypeForm: 'input',
        Required: true,
        showlist: false,
        IndexOrderList: 2,
        IndexOrderForm: 2,
        ErrorShowForm: '', 
        flex: 'flex: 0 0 15%;'
       }, 
       {
          TypeDato: 'string',
          KeyJSON: 'permisos',
          HeadShowList: 'Permisos',
          TypeForm: 'select',
          Required: true,
          showlist: false,
          IndexOrderList: 3,
          IndexOrderForm: 3,
          ErrorShowForm: 'Permisos es requerido', 
          flex: 'flex: 0 0 15%;'
        },
       
        {
          TypeDato: 'string',
          KeyJSON: 'Descripcion',
          HeadShowList: 'Descripción',
          TypeForm: 'input',
          Required: true,
          showlist: true,
          IndexOrderList: 4,
          IndexOrderForm: 4,
          ErrorShowForm: 'Descripción es Requerido', 
          flex: 'flex: 0 0 15%;'
        },
        {
          TypeDato: 'string',
          KeyJSON: 'Note',
          HeadShowList: 'Nota',
          TypeForm: 'input',
          Required: false,
          showlist: false,
          IndexOrderList: 5,
          IndexOrderForm: 5,
          ErrorShowForm: '', 
          flex: 'flex: 0 0 15%;'
        }
    ]
  }
  idEmpresa: any;

  constructor( private rolesServices: RolesService, 
               private _FinfAnyService:FinfAnyService,
               private userListService: UserListService,
               private empresalistservice: EmpresaListService,) 
  {
    Promise.all([this.GetEmpresa(),this.GetPermisos(),this.GenerateListUser()])
    .then((res)=>this.FindDBRol())//busco todo los roles, usuarios y permisos del cliente
    .catch((error)=>console.error(error));
   }

  ngOnInit(): void {
  }

  DataSalida(event) {
    console.log(event);
    switch (event.Option) {
      case 0:
        console.log("editar");
        // Opcion 0 es para la edicion de un rol

        break;
      case 1:
        console.log("eliminar");
        // Opcion 1 para la eliminacion de un rol
        break;

      default:
        console.log("creacion");
        console.log(event.Option);
        // Por defecto la creacion de un rol
       //
        this.CreateRol(event.Data) //Aun no esta al 100%
        break;
    }
  }

  createForm(datos: any) {
    datos.forEach(d => {
      /*             
      this.setForm.push({
        "ID": "idUser",
        "Label": "idUser",
        "Type": "input",
        "Data": "",
        "Value": "",
        "Required": false,
        "showlist":true
      })
      */
    });
  }

  //Obtengo la idCuenta que maneja
  GetEmpresa() {
    try {
      //No hay promesa porque el GetIdEmpresaActiva es un return;
      this.idEmpresa =this.empresalistservice.GetIdEmpresaActiva();
    } catch (error) {
      console.error(error)
    }
  }

  //Creo un rol
  CreateRol(Data:any) {
    let UserToSave:any=[];
    let PermisoToSave:any=[];
    // DATA ESTATICA PARA LA CREACION DE UN ROL
    // CAMBIAR DATOS ANTES DE HACER CADA PRUEBA 
    // NO MANADAR DATOS EN idRolU y idRolPerU
    // MANDARLOS COMO STRING '' VACIOS
  //   return {
  //     "idRolU": "",
  //     "idEmpresa": 0,
  //     "Descripcion": "User",
  //     "Note": "soy un permiso",
  //     "rpRolesPermisos": [
  //         {
  //             "idRolPerU": "",
  //             "idRolU": "",
  //             "idPermiso": 1
  //         }
  //     ]
  // }
   Data.forEach((DatosToSave:any,index:number) => {
     console.log("DatosToSave");
     console.log(DatosToSave.ID);
    if(DatosToSave.ID=='Usuarios'){
      //Separo primero para saber cual es el correo
      DatosToSave.Value.forEach( (User:any)=> {
        let DataSplit=User.split('-');
        //Ahora busco en la data de lo usuarios para saber el id del mismo
        this.Users.forEach((DataUser:any,index:any) => {
          if(DataUser.Email == DataSplit[1]){
            UserToSave.push(DataUser.idUsers);
          }
        });
        console.log("Usuarios a actualizar");
        console.log(UserToSave);
      });
    }

    if(DatosToSave.ID=='permisos'){
        DatosToSave.Value.forEach( (PermisoSelect:any)=> {
          //Ahora busco en la data de los permisos
          this.Permisos.forEach((Permiso:any,index:any) => {
            if(Permiso.Descripcion == PermisoSelect){
              PermisoToSave.push(Permiso.idPermisos);
            }
          });
          console.log("Permisos Actualizar");
          console.log(PermisoToSave);
        });
      }
   });

   let DataDBSaveRol:any={};
    Data.forEach(element => {
      DataDBSaveRol ={
        idRolU:"",
        idEmpresa:element.ID.hasOwnProperty('idEmpresa')?element.Data:0,
        Descripcion:element.ID.hasOwnProperty('Descripcion')?element.Data:"",
        Note:element.ID.hasOwnProperty('Note')?element.Data:"",
        rpRolesPermisos: [
            {
                idRolPerU: "",
                idRolU: "",
                idPermiso: 1
            }
        ]
      }
    });

    // this.rolesServices.createRoles(DataDBSaveRol)
    // .subscribe((res) => {
    //   console.log(res);
    // });


  //COMENTADO TEMPORALMENTE
    // let DataDBSaveRol:any={};
    // Data.forEach(element => {
    //   DataDBSaveRol ={
    //     idRolU:"",
    //     idEmpresa:element.ID.hasOwnProperty('idEmpresa')?element.Data:0,
    //     Descripcion:element.ID.hasOwnProperty('Descripcion')?element.Data:"",
    //     Note:element.ID.hasOwnProperty('Note')?element.Data:0,
    //     rpRolesPermisos: [
    //         {
    //             idRolPerU: "",
    //             idRolU: "",
    //             idPermiso: 1
    //         }
    //     ]
    //   }
    // });

      /*
      this.rolesServices.createRoles()
              .subscribe((res) => {
                console.log(res);
              });
      */
  }

  //Busco los roles
  FindDBRol(){
    this._FinfAnyService.table = "rpRoles"
    this._FinfAnyService.Campo = "idEmpresa"
    this._FinfAnyService.Valor = this.idEmpresa//Esta debe ser dinamica
    try {
          this._FinfAnyService.getAllData().subscribe((res) => {                      
            if (Array.isArray(res) && res.length > 0) {
              res.forEach((element:any,index:number )=> {
                element.permisos =this.PermisosToForm;
                element.Usuarios = this.ListaUserForm;
                //"Data": "Uno/Dos/Tres/Cuatro/Cinco" Asi es la data para un select
                if(index == res.length-1){
                  console.log("Roles");
                  console.log(res);
                  this.dataTable2 = res;
                  this.showTable = true;
                }
              });
            }

            });
      } catch (error) {
            console.error(error)
    } 
  }

  //Obtengo los permisos
  GetPermisos(){
    return new Promise((resolve, reject) => {
      this._FinfAnyService.table = "rpPermisos"
      this._FinfAnyService.Campo = "url"
      this._FinfAnyService.Valor = 1
      try {
            this._FinfAnyService.getAllData().subscribe((res) => {
              if ( Array.isArray(res) && res.length > 0) {
                //Esta logica es para traer los permisos y colocar en un variable temporal los datos
                //como lo necesita el select para funcionar ej "usuario/administrador/restringido"
                let Temp;
                res.forEach((element:any,index:number) => {
                  if(index == 0){
                    Temp = element.Descripcion;
                  }else{
                    Temp = Temp+'/'+element.Descripcion
                  }
                  if(res.length-1===index){ 
                    console.log("TErmine con los permisos");
                    this.PermisosToForm= Temp;
                    this.Permisos = res;
                    resolve(res);
                  }
                });
                
              }else{
                reject(res)
              }
      
  
              });
        } catch (error) {
              reject(error);
              console.error(error)
      } 
    })
    
  }

  //Obtengo los usuarios segun corresponda
  GenerateListUser() {
    return new Promise((resolve, reject) => {
      try {
        this.userListService.getAllUserByidCuenta().subscribe((res) => {
  
          if (Array.isArray(res) && res.length > 0) {
              let Temp;
              res.forEach((dataUser:any,index:number) => {
                if(index == 0){
                  Temp = dataUser.NameUser+'-'+dataUser.Email
                }else{
                  Temp = Temp+'/'+dataUser.NameUser+'-'+dataUser.Email
                }
                if(res.length-1===index){
                  this.ListaUserForm= Temp;
                  this.Users = res;
                  resolve(res);
                }
              });
          }else{
            reject(res)
          }
  
        });
      } catch (error) {
        console.error(error);
        reject(error);
      }
    })
    

  }

}

