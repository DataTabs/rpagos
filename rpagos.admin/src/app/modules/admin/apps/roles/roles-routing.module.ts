import { Route } from '@angular/router';
import { RolesComponent } from './roles.component';

export const RolesRoutes: Route[] = [
    {
        path     : '',
        component: RolesComponent
    }
];
