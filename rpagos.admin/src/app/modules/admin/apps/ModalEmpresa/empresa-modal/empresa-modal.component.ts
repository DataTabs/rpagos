import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './empresa-modal.component.html',
  styleUrls: ['./empresa-modal.component.scss']
})
export class EmpresaModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EmpresaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log("Data que llego al componente empresamodal")
    console.log(this.data)
  }

}
