import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SharedModule } from 'app/shared/shared.module';

import { MasterListAddModule } from 'app/modules/admin/apps/Master/Report/MasterListAdd/MasterListAdd/MasterListAdd.module';

import { UsersComponent } from './users.component';
import { UsersRoutes } from './users.routing';

import { ModalPermisosComponent } from './modal-permisos/modal-permisos.component';

import { SelectEmpresaModule } from '../select-empresa/select-empresa.module';
import { SelectPermisosModule } from '../select-permisos/select-permisos.module';

@NgModule({
  imports: [
    MasterListAddModule,
    CommonModule,
    RouterModule.forChild(UsersRoutes),
    DragDropModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatMomentDateModule,
    MatProgressBarModule,
    SharedModule,
    SelectEmpresaModule,
    SelectPermisosModule
  ],
  declarations: [
    UsersComponent,
    ModalPermisosComponent
  ],
  entryComponents: [
    ModalPermisosComponent,
    SelectEmpresaModule,
    SelectPermisosModule
  ]
})
export class UsersModule { }
