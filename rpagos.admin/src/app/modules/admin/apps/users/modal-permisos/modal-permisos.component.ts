import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserListService } from 'app/services/request/user-list/user-list.service';

@Component({
  templateUrl: './modal-permisos.component.html',
  styleUrls: ['./modal-permisos.component.scss']
})
export class ModalPermisosComponent implements OnInit {

  DataUsuario: any

  constructor(
    public dialogRef: MatDialogRef<ModalPermisosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _UserService: UserListService
  ) { }

  ngOnInit(): void {
    console.log("Data que llego al componente empresamodal")
    console.log(this.data)
    this.DataUsuario = this.data
  }

  getEmpresasSelect(DEmpresa) {

    console.log("Empresas Seleccionadas")
    DEmpresa.forEach(element => {
      console.log(element);
      this.DataUsuario.idEmpresaActiva = element.idEmpresa
    });

  }

  getRolSelect(DRol) {

    console.log("Rol select in modal")
    console.log(DRol);

    this.DataUsuario.idRol = DRol.value.toString()

  }

}
