/* import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {  },
];

export const PlantillasRoutes = RouterModule.forChild(routes); */
import { Route } from '@angular/router';
import { UsersComponent } from './users.component';

export const UsersRoutes: Route[] = [
    {
        path     : '',
        component: UsersComponent
    }
];
