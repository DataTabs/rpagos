import { Component, OnInit } from '@angular/core';
import { DatosListGeneric } from '../Master/DatosListGeneric';

import { UserListService } from '../../../../services/request/user-list/user-list.service';

import { EmpresaListService } from 'app/services/request/empresa-list/empresa-list.service';

import { MatDialog } from '@angular/material/dialog';
import { ModalPermisosComponent } from './modal-permisos/modal-permisos.component';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  showTable: boolean = false

  setForm: any = []
  dataTable: any = []

  /*{
    Avater: "assets/images/avatars/brian-hughes.jpg",
    Email: "vestibulum.ante@hotmail.couk",
    LoginUser: "in.condimentum.donec@protonmail.org",
    Movil: "1-691-272-8414",
    NameUser: "Lysandra Daniel",
    PassUser: "NKD01OQU8GV",
    Status: 0,
    createdAt: "2021-12-17T10:58:22.000Z",
    idCuenta: 3,
    idRol: 1,
    idUsers: 3,
    updatedAt: "2022-10-19T17:43:41.000Z"
  }*/

  public Parametros: DatosListGeneric = {
    Title: "Users",          // Titulo para mostrar
    TableName: "rpUsers",      // Nombre de la Tabla en Base de Datos / Inmemory
    Menu: true,                 // Indica si la lista tiene Menú
    MultiSelect: false,         // Seleccion multiple en la lista
    ArgMenu: ["Permisos/fingerprint", "Editar/edit", "Eliminar/delete"], //  // Menú de Opciones en la lista
    DownloasExcel: true,
    ConfigCampos: [/* {
      TypeDato: 'avatar',     // Tipo de Datos para la Lista {string, phone, date, icon, avatar, img}
      KeyJSON: 'Avater',      // El canpo que viene en el json
      HeadShowList: 'Avatar', // Title de la lista
      TypeForm: 'avatar', // Tipo de Dato para el Formulario
      Required: false,        // Si el campo es requerido para el Form
      showlist: false,         // Si se va a mostrar en la lista
      IndexOrderList: 0,      // En que orden va salir en la lista
      IndexOrderForm: 0,      // En que orden va salir en el Formulario
      ErrorShowForm: ''       // Texto en caso de Error
    },  */
      {
        TypeDato: 'string',
        KeyJSON: 'NameUser',
        HeadShowList: 'Nombre y Apellido',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Nombre es Requerido', 
        flex: 'flex: 0 0 25%;'
      },
      {
        TypeDato: 'string',
        KeyJSON: 'Email',
        HeadShowList: 'Email',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Agente es Requerido', 
        flex: 'flex: 0 0 25%;'
      },
      {
        TypeDato: 'string',
        KeyJSON: 'Movil',
        HeadShowList: 'Movil',
        TypeForm: 'input',
        Required: false,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Cola es Requerido', 
        flex: 'flex: 0 0 20%;'
      },
      {
        TypeDato: 'status',
        KeyJSON: 'Status',
        HeadShowList: 'Estatus',
        TypeForm: 'select',
        Required: false,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Estatus es Requerido', 
        flex: 'flex: 0 0 15%;'
      }
    ]
  }
 

  constructor(
    private userListService: UserListService,
    private empresalistservice: EmpresaListService,
    public _dialog: MatDialog,) {
    this.GenerateList()
    this.GetEmpresa()
  }

  ngOnInit(): void {
  }

  GenerateList() {
    try {
      this.userListService.getAllUserByidCuenta().subscribe((res) => {
        console.log("Usuarios")
        console.log(res)
        //console.log(res.length)

        if (res.length > 0) {
          this.dataTable = res
          this.showTable = true
        }

      });
    } catch (error) {
      console.error(error)
    }

  }

  GetEmpresa() {
    try {
      // Esto se debe llemar luego del Login
      this.empresalistservice.GetAllEmpresaByidCuenta().subscribe((res: any) => {
        console.log("Empresa")
        console.log(res)
        // Esto estaba dañando la consulta de roles PILAS! Jhonattan codigo era de Juan
        //this.empresalistservice.EmpresasAcceso = res
      })

    } catch (error) {
      console.error(error)
    }
  }

  DataSalida(event) {
    console.log("esto me llego: ", event)

    let Datos = event.Data

    switch (event.Option) {
      case 0:   // Permisos
        // Aqui va el dialogo Juan me debe listar las empresas para seleccionar a las que tiene permiso el usuario.
        console.log("Permisos")
        const dialogRef = this._dialog.open(ModalPermisosComponent, {
          data: event.Data,
          width: '350px',
          panelClass: 'custom-dialog-container'
        }).afterClosed().subscribe(result => {
          if (result) {
            console.log(result)
            console.log("Se guardo")

            this.userListService.updateInfoUser(result).subscribe((res) => {
              console.log(res);
            });

          }
        });
        break;
      case 1:
        //Editar
        console.log('Editar: ', event.Data)
        break;
      case 2:
        //Delete
        console.log('delete: ', event.Data)
        break;
      default:

        let UserData: any = this.dataTable.find((DUser: any) => DUser.NameUser === Datos[0].Data)

        console.log("Data del usuario a modificar ")
        console.log(UserData)


        let Datafinal: any = {}
        for (let indexEvent = 0; indexEvent < Datos.length; indexEvent++) {
          let Data = Datos[indexEvent]
          //console.log(Data)

          let NameCampo
          let ValorCampo

          for (var key in Data) {
            if (key === "ID") {
              NameCampo = Data[key]
            }
            if (key === "Value") {
              ValorCampo = Data[key]
            }

          }
          if (NameCampo && ValorCampo) {
            UserData[NameCampo] = ValorCampo
            console.log(NameCampo + ": " + ValorCampo)
          }
        }

        //console.log(UserData)
        this.UpdateUser(UserData)
        break;
    }
  }

  createForm(datos: any) {
    datos.forEach(d => {
      /*             this.setForm.push({
              "ID": "idUser",
              "Label": "idUser",
              "Type": "input",
              "Data": "",
              "Value": "",
              "Required": false,
              "showlist":true
                   } ) */

    });

  }

  UpdateUser(Data: any) {
    try {

      console.log(Data)

      this.userListService.updateInfoUser(Data).subscribe((res) => {
        console.log(res);
      });

    } catch (error) {
      console.error(error)
    }
  }

}
