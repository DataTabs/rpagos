import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { Contact, Country, Tag, TagSocial, TagEmails, TagPhone, CountryFlag } from 'app/modules/admin/apps/contacts/contacts.types';
import { client,extras } from 'models/client.model';
import { ClientesRequestService } from 'app/services/request/clientes/clientes-request.service';
import { FuseMockApiUtils } from '@fuse/lib/mock-api';
import { EmpresaListService } from 'app/services/request/empresa-list/empresa-list.service';
import {isEmpty} from 'lodash';
@Injectable({
    providedIn: 'root'
})
export class ContactsService
{
    // Private
    private _contact: BehaviorSubject<any | null> = new BehaviorSubject(null);
    private _contacts: BehaviorSubject<any[] | null> = new BehaviorSubject(null);
    private _clientes: BehaviorSubject<any[] | null> = new BehaviorSubject(null);
    private _countries: BehaviorSubject<Country[] | null> = new BehaviorSubject(null);
    private _countriesFlag: BehaviorSubject<CountryFlag[] | null> = new BehaviorSubject(null);
    private _tags: BehaviorSubject<Tag[] | null> = new BehaviorSubject(null);
    private _tagsSocials: BehaviorSubject<TagSocial[] | null> = new BehaviorSubject(null);
    private _tagsEmails: BehaviorSubject<TagEmails[] | null> = new BehaviorSubject(null);
    private _tagsPhones: BehaviorSubject<TagPhone[] | null> = new BehaviorSubject(null);
    private idEmpresaActiva: any
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient,
                private _ClientesRequestService:ClientesRequestService,
                private _EmpresaListService: EmpresaListService)
    {

        console.log(_EmpresaListService.EmpresasAcceso)
        this.idEmpresaActiva =  _EmpresaListService.EmpresasAcceso
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for contact
     */
    get contact$(): Observable<Contact>
    {
        return this._contact.asObservable();
    }

    /**
     * Getter for contacts
     */
    get contacts$(): Observable<Contact[]>
    {
        return this._contacts.asObservable();
    }

    /**
     * Getter for countries
     */
    get countries$(): Observable<Country[]>
    {
        return this._countries.asObservable();
    }
    /**
     * Getter for countries
     */
    get countriesFlag$(): Observable<CountryFlag[]>
     {
         return this._countriesFlag.asObservable();
     }

    /**
     * Getter for tags
     */
    get tags$(): Observable<Tag[]>
    {
        return this._tags.asObservable();
    };
    get tagsSocials$(): Observable<TagSocial[]>
    {
        return this._tagsSocials.asObservable();
    }
    get tagsEmails$(): Observable<TagEmails[]>
    {
        return this._tagsEmails.asObservable();
    }
    get tagsPhone$(): Observable<TagPhone[]>
    {
        return this._tagsPhones.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get contacts
     */
    getContacts(): Observable<any[]>
    {
        //console.log('me mandaron')
        //return this._httpClient.get<Contact[]>('api/apps/contacts/all').pipe(
        // return this._httpClient.get<any[]>('api/apps/contacts/all').pipe(
        return this.getClientes().pipe(
             map((contact:any[]) => {
                  console.log(contact, contact.length);
                 if (!contact.length)return []
                    let result =contact.map((contact)=> this.buildClientfromdb(contact))
                    return result.sort((a, b) => a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1)
            }
            ),
            tap((contacts) => {
                 console.log(contacts)
                this._contacts.next(contacts);
            })
        );
    }

    /**
     * Search contacts with given query
     *
     * @param query
     */
    searchContacts(query: string): Observable<Contact[]>
    {
        //if (isEmpty(query)) this.getContacts();
        return this._contacts.pipe(
            map((contacts) => {
                // Find the contact
                return  contacts.filter(item => item.name.toLowerCase().includes(query.toLowerCase())) || null;
            }),
            tap((contacts) => {
                //console.log(contacts)
                this._contacts.next(contacts);
            })
        );
        //return this._httpClient.get<Contact[]>('api/apps/contacts/search', {
            //params: {query}
        //}).pipe(
            //tap((contacts) => {
                //this._contacts.next(contacts);
            //})
        //);
    }

    /**
     * Get contact by id
     */
    getContactById(id: string): Observable<Contact>
    {
        //console.log('get this: ',id)
        return this._contacts.pipe(
            take(1),
            map((contacts) => {
                // Find the contact
                const contact = contacts.find(item => item.id === id) || null;

                // Update the contact
                console.log(contact)
                this._contact.next(contact);

                // Return the contact
                return contact;
            }),
            switchMap((contact) => {

                if ( !contact )
                {
                    return throwError('Could not found contact with id of ' + id + '!');
                }

                return of(contact);
            })
        );
    }

    /**
     * Create contact
     */
    createContact(): Observable<Contact>
    {
        return this.contacts$.pipe(
            take(1),
            switchMap(contacts => of<any>({
                id          : FuseMockApiUtils.guid(),
                avatar      : null,
                name        : 'New Contact',
                emails      : [],
                phoneNumbers: [],
                job         : {
                    title  : '',
                    company: ''
                },
                birthday    : null,
                address     : null,
                notes       : null,
                tags        : [],
                tagsSocials : [],
                tagsPhone   : [],
                tagsEmails  : []
            }).pipe(
                map((newContact) => {

                    // Update the contacts with the new contact
                    this._contacts.next([newContact, ...contacts]);

                    // Return the new contact
                    return newContact;
                })
            ))
        );
    }

    /**
     * Update contact
     *
     * @param idid: string,
     * @param contact
     */
    updateContact( contact: Contact, newContact?:boolean): Observable<Contact>
    {
        console.log(contact)
        if (!newContact){
        console.log('update',contact)
        return this.contacts$.pipe(
            take(1),
            switchMap(contacts =>  this.updateCliente(this.buildClient(contact))//this._ClientesRequestService.updateCliente(contact)
            .pipe(
                map((updatedContact) => {
                    console.log(this.buildClient(contact))

                    // Find the index of the updated contact
                    const index = contacts.findIndex(item => item.id === contact.id);
                    //console.log("este es el index del contact " + index,contacts[index] )
                    // Update the contact
                    contacts[index] = contact;

                    // Update the contacts
                    this._contacts.next(contacts);

                    // Return the updated contact
                    return contact;
                }),
                switchMap(updatedContact => this.contact$.pipe(
                    take(1),
                    filter(item => item && item.id === contact.id),
                    tap(() => {

                        // Update the contact if it's selected
                        this._contact.next(updatedContact);

                        // Return the updated contact
                        return contact;
                    })
                ))
            ))
        );

        }else{
        //console.log(contact)
        console.log('create')
        return this.contacts$.pipe(
            take(1),
            switchMap(contacts =>  this.createCliente(this.buildClient(contact,true))//this._ClientesRequestService.updateCliente(contact)
            .pipe(
                map((updatedContact) => {
                    //console.log(updatedContact)

                    // Find the index of the updated contact
                    const index = contacts.findIndex(item => item.id === contact.id);
                    //console.log("este es el index del contact " + index,contacts[index] )
                    // Update the contact
                    contacts[index] = contact;

                    // Update the contacts
                    this._contacts.next(contacts);

                    // Return the updated contact
                    return contact;
                }),
                switchMap(updatedContact => this.contact$.pipe(
                    take(1),
                    filter(item => item && item.id === contact.id),
                    tap(() => {

                        // Update the contact if it's selected
                        this._contact.next(updatedContact);

                        // Return the updated contact
                        return contact;
                    })
                ))
            ))
        );

        }

    }

    /**
     * Delete the contact
     *
     * @param id
     */
    deleteContact(id: string): Observable<boolean>
    {
        return this.contacts$.pipe(
            take(1),
            switchMap(contacts => this._httpClient.delete('api/apps/contacts/contact', {params: {id}}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted contact
                    const index = contacts.findIndex(item => item.id === id);

                    // Delete the contact
                    contacts.splice(index, 1);

                    // Update the contacts
                    this._contacts.next(contacts);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }

    /**
     * Get countries
     */
    getCountries(): Observable<Country[]>
    {
        return this._httpClient.get<Country[]>('api/apps/contacts/countries').pipe(
            tap((countries) => {
                this._countries.next(countries);
            })
        );
    }
    /**
     * Get countries
     */
     getCountriesFlag(): Observable<CountryFlag[]>
     {
         return this._httpClient.get<CountryFlag[]>('api/apps/contacts/countriesFlag').pipe(
             tap((countriesFlag) => {
                 this._countriesFlag.next(countriesFlag);
             })
         );
     }

    /**
     * Get tags
     */
    getTags(): Observable<Tag[]>
    {
        return this._httpClient.get<Tag[]>('api/apps/contacts/tags').pipe(
            tap((tags) => {
                this._tags.next(tags);
            })
        );
    }
    getTagsSocials(): Observable<TagSocial[]>
    {
        return this._httpClient.get<TagSocial[]>('api/apps/contacts/tagsSocials').pipe(
            tap((tagsSocials) => {
                this._tagsSocials.next(tagsSocials);
            })
        );
    }
    getTagsEmails(): Observable<TagEmails[]>
    {
        return this._httpClient.get<TagEmails[]>('api/apps/contacts/tagsEmails').pipe(
            tap((tagsEmails) => {
                this._tagsEmails.next(tagsEmails);
            })
        );
    }
    getTagsPhones(): Observable<TagPhone[]>
    {
        return this._httpClient.get<TagPhone[]>('api/apps/contacts/tagsPhones').pipe(
            tap((tagsPhones) => {
                this._tagsPhones.next(tagsPhones);
            })
        );
    }

    /**
     * Create tag
     *
     * @param tag
     */
    createTag(tag: Tag): Observable<Tag>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.post<Tag>('api/apps/contacts/tag', {tag}).pipe(
                map((newTag) => {

                    // Update the tags with the new tag
                    this._tags.next([...tags, newTag]);

                    // Return new tag from observable
                    return newTag;
                })
            ))
        );
    }

    /**
     * Update the tag
     *
     * @param id
     * @param tag
     */
    updateTag(id: string, tag: Tag): Observable<Tag>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.patch<Tag>('api/apps/contacts/tag', {
                id,
                tag
            }).pipe(
                map((updatedTag) => {

                    // Find the index of the updated tag
                    const index = tags.findIndex(item => item.id === id);

                    // Update the tag
                    tags[index] = updatedTag;

                    // Update the tags
                    this._tags.next(tags);

                    // Return the updated tag
                    return updatedTag;
                })
            ))
        );
    }

    /**
     * Delete the tag
     *
     * @param id
     */
    deleteTag(id: string): Observable<boolean>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.delete('api/apps/contacts/tag', {params: {id}}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted tag
                    const index = tags.findIndex(item => item.id === id);

                    // Delete the tag
                    tags.splice(index, 1);

                    // Update the tags
                    this._tags.next(tags);

                    // Return the deleted status
                    return isDeleted;
                }),
                filter(isDeleted => isDeleted),
                switchMap(isDeleted => this.contacts$.pipe(
                    take(1),
                    map((contacts) => {

                        // Iterate through the contacts
                        contacts.forEach((contact) => {

                            const tagIndex = contact.tags.findIndex(tag => tag === id);

                            // If the contact has the tag, remove it
                            if ( tagIndex > -1 )
                            {
                                contact.tags.splice(tagIndex, 1);
                            }
                        });

                        // Return the deleted status
                        return isDeleted;
                    })
                ))
            ))
        );
    }

    /**
     * Update the avatar of the given contact
     *
     * @param id
     * @param avatar
     */
    uploadAvatar(id: string, avatar: File): Observable<Contact>
    {
        return this.contacts$.pipe(
            take(1),
            switchMap(contacts => this._httpClient.post<Contact>('api/apps/contacts/avatar', {
                id,
                avatar
            }, {
                headers: {
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    'Content-Type': avatar.type
                }
            }).pipe(
                map((updatedContact) => {

                    // Find the index of the updated contact
                    const index = contacts.findIndex(item => item.id === id);

                    // Update the contact
                    contacts[index] = updatedContact;

                    // Update the contacts
                    this._contacts.next(contacts);

                    // Return the updated contact
                    return updatedContact;
                }),
                switchMap(updatedContact => this.contact$.pipe(
                    take(1),
                    filter(item => item && item.id === id),
                    tap(() => {

                        // Update the contact if it's selected
                        this._contact.next(updatedContact);

                        // Return the updated contact
                        return updatedContact;
                    })
                ))
            ))
        );
    }


    /**
     * Recibe la data de clientes y retorna estructura de Clientes para bd
     * @param {data} Data desde componeten
     * @returns {Cliente} Cliente para bd
     */
    buildClient(data: any, create?:boolean){
        console.log(data)
          let cliente:any={
            _id:0,
            idCliente: '',
            idEmpresa: '',
            CodeCliente: FuseMockApiUtils.guid(),
            ClienteName: '',
            rif_nit: '',
            avatar: '',
            StatusC: 0,
            idPais: 0,
            idCiudad: 0,
            rpClientesExtras: [],
            nota: ''
        }

        !create && delete cliente.CodeCliente

        cliente.idCliente=data.idCliente||''
        cliente.idEmpresa=data.idEmpresa ||0
        //cliente.CodeCliente="0123"
        cliente.idTipoClientes=1
        cliente.ClienteName=data.name
        cliente.name=data.title
        cliente.rif_nit=data.company //TODO: Cambiar este nombre de atributo
        cliente.avatar=data.avatar
        cliente.StatusC=0
        cliente.idPais=13
        cliente.idCiudad=34
        cliente.nota=data.notes

        data.address.length>0 && cliente.rpClientesExtras.push({
            idCliente: data._id ||'',
            idExtraCliente:data.idAddress || '',
            TipoDato: 1,
            Descrip:data.address,
            Status:0
        })

        data.addressShop.length>0 && cliente.rpClientesExtras.push({
            idCliente: data._id ||'',
            idExtraCliente:data.idaddressShop || '',
            TipoDato: 1,
            Descrip:data.addressShop,
            Status:0
        })

        data.emails.length>0 && data.emails.forEach(email => cliente.rpClientesExtras.push({
            idCliente: email._id ||'',
            idExtraCliente:email.idExtraCliente || '',
            TipoDato: 2,
            Descrip:email.email,
            // ExtraTipoDato:email.label,
            Status:0
        }
        ))

        data.phoneNumbers.length>0 && data.phoneNumbers.forEach(phoneNumber => cliente.rpClientesExtras.push({
            idCliente: phoneNumber._id ||'',
            idExtraCliente:phoneNumber.idExtraCliente || '' ,
            TipoDato: 3,
            Descrip:phoneNumber.phoneNumber, //TODO Falta el codigo de pais
            // ExtraTipoDato:phoneNumber.label,
            Status:0

        }))

        data.redes.length>0 && data.redes.forEach(redes => cliente.rpClientesExtras.push({
            idCliente: redes._id ||'',
            idExtraCliente:redes.idExtraCliente,
            TipoDato: 4,
            Descrip:redes.name, //TODO Falta el codigo de pais
            // ExtraTipoDato:redes.label,
            Status:0
        }))
         console.log(cliente)

         return cliente
    }

    /**
     * Recibe la data de clientes y retorna estructura de Clientes para bd
     * @param {data} Data desde componeten
     * @returns {Cliente} Cliente para bd
     */
    buildClientfromdb(data: any){
        console.log("buildClientfromdb : ", data,data._id)

        let cliente={
            _id:0,
            id: "",
            idCliente:"",
            idEmpresa:"",
            avatar: "",
            background: "",
            name: "",
            title: "",
            company:"",
            birthday:"",
            address: "",
            idAddress:"",
            addressShop:"",
            idaddressShop:"",
            notes:"",
            emails: [
            ],
            phoneNumbers: [
            ],
            paisporusuario: [
            ],
            tags: [
            ],
            tagsSocials: [
            ],
            tagsEmails: [
            ],
            tagsPhone: [
            ]
        };

        cliente._id=data._id ||""
        cliente.idCliente=data.idCliente ||""
        cliente.idEmpresa=data.idEmpresa ||""
        cliente.id=data.idCliente.toString()
        cliente.name=data.ClienteName
        //codeCliente?
        //idTipoClientes?
        cliente.avatar= data.avatar
        cliente.company= data.rif_nit || ""
        cliente.title = data.ClienteName
        //idPais=data.idPais
        //idCiudad
        //StatusC
        cliente.notes = data.nota
        data.rpClientesExtras.length>0 &&
        data.rpClientesExtras.forEach((extra) => {
            //  console.log("extra: " + JSON.stringify(extra))
            // console.log(extra.idExtraCliente)
            switch (extra.TipoDato) {
                case 1:
                    //address
                    cliente.address = extra.Descrip
                    cliente.idAddress = extra.idExtraCliente
                    break;

                case 2:
                    //email
                   cliente.emails.push({
                   email: extra.Descrip,
                   label: "",
                   idExtraCliente:extra.idExtraCliente

                    })
                    break;

                case 3:
                    //phoneBumber
                    //TODO : revisar en componente de clientes
                    cliente.phoneNumbers.push({
                    country: "",
                    phoneNumber: extra.Descrip,
                    label: "",
                   idExtraCliente:extra.idExtraCliente
                    })
                    break;

                case 4:
                    //redes

                    break;
                default:
                    break;
            }
        })


        //console.log("Asi quedo el cliente: " + cliente)

        return cliente
    }

    getClientes():Observable<any>{
        //TODO Enviar idEmpresa de manera dinamica
        return this._ClientesRequestService.getClientesAll(1)//this.idEmpresaActiva
    }

    createCliente(cliente:any):Observable<any>{
        return this._ClientesRequestService.addCliente(cliente)
    }

    updateCliente(cliente:any):Observable<any>{
        return this._ClientesRequestService.updateCliente(cliente)
    }

}
//!Asi hay que setear para pasar en db
/*
{
    "idCliente": 1,
    "idEmpresa": 1,
    "CodeCliente": "099900",
    "idTipoClientes": 1,
    "ClienteName": "EDU",
    "avatar": null,
    "rif_nit": null,
    "idPais": 3,
    "idCiudad": 2,
    "StatusC": 5,
    "nota": "una nota",
    "createdAt": "2021-10-19T23:29:45.000Z",
    "updatedAt": "2021-11-06T21:05:02.000Z",
    "rpClientesExtras": [
        {
            "idExtraCliente": 0,
            "idCliente": 1,
            "TipoDato": 2,
            "Descrip": "LEER!",
            "Status": 0,
            "createdAt": "2021-09-25T21:51:24.000Z",
            "updatedAt": "2021-09-25T21:51:24.000Z"
        },
        {
            "idExtraCliente": 1,
            "idCliente": 1,
            "TipoDato": 23,
            "Descrip": "LAPIZ",
            "Status": 0,
            "createdAt": "2021-09-25T21:51:24.000Z",
            "updatedAt": "2021-09-25T21:51:24.000Z"
        }
    ]
}
*/

//!Asi hay que setear para mostrar en clientes
/*
{
    "id": "22f18d47-ff8d-440e-888d-a1747c093052",
    "avatar": "assets/images/avatars/female-12.jpg",
    "background": "assets/images/cards/14-640x480.jpg",
    "name": "Alice Harding",
    "title": "Track Service Worker",
    "company": "Futurity",
    "birthday": "1985-09-17T12:00:00.000Z",
    "address": "387 Holt Court, Thomasville, Alaska, PO2867",
    "addressShop": "Sabana Grande, Caracas, Venezuela, 2612",
    "notes": "<p>Adipisicing exercitation dolor nisi ipsum nostrud anim dolore sint veniam consequat lorem sit ex commodo nostrud occaecat elit magna magna commodo incididunt laborum ad irure pariatur et sit ullamco adipisicing.</p><p>Ullamco in dolore amet est quis consectetur fugiat non nisi incididunt id laborum adipisicing dolor proident velit ut quis aliquip dolore id anim sit adipisicing nisi incididunt enim amet pariatur.</p>",
    "emails": [
        {
            "email": "aliceharding@mail.us",
            "label": "Personal"
        }
    ],
    "phoneNumbers": [
        {
            "country": "sx",
            "phoneNumber": "881 472 3113",
            "label": "Mobile"
        },
        {
            "country": "sx",
            "phoneNumber": "974 548 3124",
            "label": "Work"
        },
        {
            "country": "sx",
            "phoneNumber": "800 518 3615",
            "label": "Home"
        }
    ],
    "paisporusuario": [
        {
            "country": "mx"
        }
    ],
    "tags": [
        "2026ce08-d08f-4b4f-9506-b10cdb5b104f"
    ],
    "tagsSocials": [
        "c31e9e5d-e0cb-4574-a13f-8a6ee5ff8303",
        "a8991c76-2fda-4bbd-a718-df13d6478804",
        "56ddbd47-4078-4ddd-8448-73c5e88d5f05"
    ],
    "tagsEmails": [
        "c31e9e5d-e0cb-4574-a13f-8a6ee5ff8303",
        "a8991c76-2fda-4bbd-a718-df13d6478804",
        "56ddbd47-4078-4ddd-8448-73c5e88d5f05"
    ],
    "tagsPhone": [
        "c31e9e5d-e0cb-4574-a13f-8a6ee5ff8303",
        "a8991c76-2fda-4bbd-a718-df13d6478804",
        "56ddbd47-4078-4ddd-8448-73c5e88d5f05"
    ]
}
*/
