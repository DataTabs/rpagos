export interface Contact
{
    id: string;
    avatar?: string | null;
    background?: string | null;
    name: string;
    emails?: {
        email: string;
        label: string;
    }[];
    job?: {
        title  : string,
        company: string
    }
    redes?: {
        name: string;
        label: string;
    }[];
    phoneNumbers?: {
        country: string;
        phoneNumber: string;
        label: string;
    }[];
    paisporusuario?: {
        country: string;
    }[];
    countryFlag?: {
        id: string;
        iso: string;
        name: string;
        flagImagePos: string;
    }[];
    title?: string;
    company?: string;
    birthday?: string | null;
    address?: string | null;
    addressShop?: string | null;
    notes?: string | null;
    tags: string[];
    tagsSocials: string[];
    tagsEmail: string[];
    tagsPhone: string[];
    //tagsEmails: string[];
}

 // {
    //     id          : FuseMockApiUtils.guid(),
    //     avatar      : null,
    //     name        : 'New Contact',
    //     emails      : [],
    //     phoneNumbers: [],
    //     job         : {
    //         title  : '',
    //         company: ''
    //     },
    //     birthday    : null,
    //     address     : null,
    //     notes       : null,
    //     tags        : [],
    //     tagsSocials : [],
    //     tagsPhone   : [],
    //     tagsEmails  : []
    // }

export interface Country
{
    id: string;
    iso: string;
    name: string;
    flagImagePos: string;
}

export interface CountryFlag
{
    id: string;
    iso: string;
    name: string;
    code: string;
    flagImagePos: string;
}

export interface Tag
{
    id?: string;
    title?: string;
}
export interface TagSocial
{
    id?: string;
    title?: string;
}
export interface TagEmails
{
    id?: string;
    title?: string;
}
export interface TagPhone
{
    id?: string;
    title?: string;
}
