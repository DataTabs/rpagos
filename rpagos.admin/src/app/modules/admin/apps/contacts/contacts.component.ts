import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { DTableService } from 'app/services/dtable.service';

@Component({
    selector: 'contacts',
    templateUrl: './contacts.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactsComponent {
    /**
     * Constructor
     */
    constructor(private _httpclient: HttpClient, private _DTable: DTableService) {

    }

    PruebaAgregar() {
        //const datos = { item: { id: "1" }, id: "1", apellido: 'Lobo', nombre: 'Juan' }
        const datos = [
            { item: { id: "1" }, id: "1", apellido: 'Lobo', nombre: 'Juan' },
            { item: { id: "2" }, id: "2", apellido: 'Perez', nombre: 'Pedro' },
            { item: { id: "3" }, id: "3", apellido: 'Lopez', nombre: 'Ivan' }
        ]
        this._DTable.AddItem("User", datos, 'new').then((res)=>{
            console.log('Resultados Obtenidos')
            console.log(res)
        })
    }

    PruebaEditar() {
        const datos = { item: { id: "1" }, id: "1", apellido: 'Lobo Guerra', nombre: 'Juan Carlos' }
        this._DTable.AddItem("User", datos, 'edit').then((res)=>{
            console.log('Resultados Obtenidos')
            console.log(res)
        })
    }

    PruebaEliminar() {
        const datos = { item: { id: "1" }, id: "1", apellido: 'Lobo Guerra', nombre: 'Juan Carlos' }
        this._DTable.AddItem("User", datos, 'delete').then((res)=>{
            console.log('Resultados Obtenidos')
            console.log(res)
        })
    }

    PruebaConsultar() {
        this._DTable.AddItem("User", {}, 'get').then((res)=>{
            console.log('Resultados Obtenidos')
            console.log(res)
        })
    }

}
