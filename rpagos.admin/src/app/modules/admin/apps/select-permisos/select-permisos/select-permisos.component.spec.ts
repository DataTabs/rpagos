import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPermisosComponent } from './select-permisos.component';

describe('SelectPermisosComponent', () => {
  let component: SelectPermisosComponent;
  let fixture: ComponentFixture<SelectPermisosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectPermisosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
