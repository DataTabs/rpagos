import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RolesService } from 'app/services/request/roles/roles.service';
@Component({
  selector: 'app-select-permisos',
  templateUrl: './select-permisos.component.html',
  styleUrls: ['./select-permisos.component.scss']
})
export class SelectPermisosComponent implements OnInit {
  @Input() DataUser: any
  @Output() onSelect = new EventEmitter();

  selected: "";
  members: any = [];
  roles: any = [];

  constructor(
    private _RolesService: RolesService
  ) {
  }

  ngOnInit(): void {
    // console.log("Esto me llego en select permisos ")
    // console.log(this.DataUser)

    if (this.DataUser) {
      this.GetRoles(this.DataUser.idEmpresaActiva)
    }
  }

  GetRoles(IdEmpresa: any) {
    try {
      // Esto se debe llemar luego del Login
      this._RolesService.getRolesByEmpresa(IdEmpresa).subscribe((DataRoles: any) => {
        // console.log("Roles")

        for (let indexRol = 0; indexRol < DataRoles.length; indexRol++) {

          if (this.DataUser.idRol === DataRoles[indexRol].idRol.toString()) {
            this.selected = DataRoles[indexRol].Descripcion
            //console.log(this.selected)
          }

          this.roles.push({
            label: DataRoles[indexRol].Descripcion,
            value: DataRoles[indexRol].idRol,
            description: DataRoles[indexRol].Note || "",
            Delete: DataRoles[indexRol].delete,
            createdAt: DataRoles[indexRol].createdAt,
            idEmpDataRolesa: DataRoles[indexRol].idEmpDataRolesa,
            rpRolesPermisos: DataRoles[indexRol].rpRolesPermisos
          })

        }
        // console.log(this.roles)
      })

    } catch (error) {
      console.error(error)
    }
  }

  checked() {
    //Aqui deberia recibir por input cuales empDataRolesas estan seleccionadas
    //this.selected[0]=this.DataEmpDataRolesa[0]
  }

  selectedRol() {
    //console.log(this.selected)
    this.outputEmiter()
  }

  outputEmiter() {
    let DataSelect = this.roles.find((DRol: any) => DRol.label === this.selected)
    //console.log(this.roles.find((DRol:any)=> DRol.label === this.selected))
    if (DataSelect) {
      this.onSelect.emit(DataSelect)
    }
  }

}
