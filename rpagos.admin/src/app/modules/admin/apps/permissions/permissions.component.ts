import { Component, OnInit } from '@angular/core';
import { FinfAnyService } from 'app/services/finf-any.service';
import { DatosListGeneric } from '../Master/DatosListGeneric';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {

  dataTable: any = []
  showTable: boolean = false

  public Parametros: DatosListGeneric = {
    Title: "Permisos",          // Titulo para mostrar
    TableName: "rpPermisos",      // Nombre de la Tabla en Base de Datos / Inmemory
    Menu: true,                 // Indica si la lista tiene Menú
    MultiSelect: false,         // Seleccion multiple en la lista
    ArgMenu: ["Editar/edit", "Eliminar/delete"], //  // Menú de Opciones en la lista
    DownloasExcel: true,
    ConfigCampos: [/* {
      TypeDato: 'avatar',     // Tipo de Datos para la Lista {string, phone, date, icon, avatar, img}
      KeyJSON: 'Avater',      // El canpo que viene en el json
      HeadShowList: 'Avatar', // Title de la lista
      TypeForm: 'avatar', // Tipo de Dato para el Formulario
      Required: false,        // Si el campo es requerido para el Form
      showlist: false,         // Si se va a mostrar en la lista
      IndexOrderList: 0,      // En que orden va salir en la lista
      IndexOrderForm: 0,      // En que orden va salir en el Formulario
      ErrorShowForm: ''       // Texto en caso de Error
    },  */
      {
        TypeDato: 'string',
        KeyJSON: 'idPermisos',
        HeadShowList: 'ID',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Seleccione un ID',         
        flex: 'flex: 0 0 15%;'
      }, {
        TypeDato: 'string',
        KeyJSON: 'Descripcion',
        HeadShowList: 'Descripción',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Agente es Descripción', 
        flex: 'flex: 0 0 60%;'
      } 
    ]
  }

  
  constructor(private _FinfAnyService:FinfAnyService) { 
    _FinfAnyService.table = "rpPermisos"
    _FinfAnyService.Campo = "url"
    _FinfAnyService.Valor = 1
    try {
          _FinfAnyService.getAllData().subscribe((res) => {
            console.log(res)
            console.log(res.length)

            if (res.length > 0) {
              this.dataTable = res
              this.showTable = true
            }

            });
      } catch (error) {
            console.error(error)
    } 
  }
    
   
  ngOnInit(): void {
  }

  DataSalida(event) {
    console.log("esto me llego: ", event)
  }

}
