import { Route } from '@angular/router';
import { PermissionsComponent } from './permissions.component';

export const PermissionsRoutes: Route[] = [
    {
        path     : '',
        component: PermissionsComponent
    }
];
