import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectEmpresasComponent } from './select-empresas.component';

describe('SelectEmpresasComponent', () => {
  let component: SelectEmpresasComponent;
  let fixture: ComponentFixture<SelectEmpresasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectEmpresasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectEmpresasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
