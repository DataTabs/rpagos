import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

import { EmpresaListService } from 'app/services/request/empresa-list/empresa-list.service';

@Component({
  selector: 'app-select-empresas',
  templateUrl: './select-empresas.component.html',
  styleUrls: ['./select-empresas.component.scss']
})
export class SelectEmpresasComponent implements OnInit {

  EmpresaSelect: any = [] //new FormControl([]);
  DataEmpresa: any = []
  selected: any

  @Input() data: any
  @Output() onSelect = new EventEmitter();


  constructor(
    private empresalistservice: EmpresaListService,
  ) {
    this.GetEmpresa()
  }

  ngOnInit(): void {
    console.log("Data del usuario en empresas select")
    console.log(this.data)
  }

  GetEmpresa() {
    try {
      this.empresalistservice.GetAllEmpresaByidCuenta().subscribe((res: any) => {
        // console.log("Empresas en select empresas")
        // console.log(res)
        this.DataEmpresa = res
        this.checked()
      })

    } catch (error) {
      console.error(error)
    }
  }

  checked() {
    //Aqui deberia recibir por input cuales empresas estan seleccionadas
    //this.selected[0]=this.DataEmpresa[0]
  }

  selectedEmp() {
    // console.log(this.selected)
    this.outputEmiter()
  }

  outputEmiter() {
    this.onSelect.emit(this.selected)
  }

}
