export interface DatosListGeneric {
        Title:      string,          // Titulo para mostrar 
        TableName: string,      // Nombre de la Tabla en Base de Datos / Inmemory
        Menu:       boolean,          // Indica si la lista tiene Menú
        MultiSelect: boolean,   // Seleccion multiple en la lista
        ArgMenu?:   any[],         // Menú de Opciones en la lista
        DownloasExcel: boolean, // Si va a permitir
        ConfigCampos:  iConfigCampos[]
}

export interface iDatosForm {
    ID: string,
    Label: string,
    Type: string,
    Data: any,
    Value: string,
    Required: boolean,
    showlist: boolean,
    ErrorLabel: string,
    Option?: any
}

export interface iConfigCampos
{
    TypeDato: string,
    KeyJSON: string,
    HeadShowList: string,
    TypeForm: string,
    Required: boolean,
    showlist: boolean,
    IndexOrderList: number,
    IndexOrderForm: number,
    ErrorShowForm: string, 
    flex: string
}
export interface iPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}