import { ChangeDetectionStrategy, ElementRef, EventEmitter, HostListener, Input, OnChanges, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ListAddType } from './ListAddType';
import { AfterViewInit, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { DatosListGeneric, iDatosForm, iPagination } from '../../../DatosListGeneric';
import { DataSource } from '@angular/cdk/collections';
import { FuseDrawerService } from '@fuse/components/drawer';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-MasterListAdd',
  templateUrl: './MasterListAdd.component.html',
  styleUrls: ['./MasterListAdd.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MasterListAddComponent implements OnInit, OnChanges {

  @Input() Param: DatosListGeneric;
  @Input() Data: any;
  esEnter = false
  private enter: number = 13;
  factura:any

  @Output() DataClick: EventEmitter<any> = new EventEmitter();

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild('filter', { static: true })
  filter: ElementRef;

  @HostListener('document:keydown', ['$event'])
  onKeyUp(ev: KeyboardEvent) {
    if (ev.key == "Enter") {
      this.esEnter = true
    } else {
      this.esEnter = false
    }
  }

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  @ViewChild('drawer', { static: false })
  drawer: MatSidenav;


  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
  // Atributos
  Title: string = "Titulo"
  isLoading: boolean = false;
  bShowMenu: boolean = false;
  ContadorColor: number = 0;
  BackColor: string = "#D6EAF8";
  pagination: iPagination;
  ldisplayedColumns: any = [];
  HeadShow: any = [];
  FlexSize: any = [];
  lTypeData: any = [];
  DataForm: iDatosForm[] = [];
  Menu: any = [];
  pageSizeOptions: number[] = [5, 10, 25, 100];
  testPaginator:any = {
    length: 1000,
    pageSize: 10,
    pageIndex: 1, 
    previousPageIndex: 0
  };
  dataSource: any// FilesDataSource | null;
  invoice = false
  // FormControl
  searchInputControl: FormControl = new FormControl();
  cShowResponsive: any;
  tableData: any;
  constructor(private _fuseDrawerService: FuseDrawerService, private _matDialog: MatDialog) {
    try {
      this.Title = this.Param.Title
    } catch (error) {

    }

    
  }


  GetOnlyOneKey(Json: any, KayName: string) {
          let ArgSalidaTemp: any[] = []
          Json.ConfigCampos.forEach(element => {
            ArgSalidaTemp.push(element[KayName])
          });
          if (this.bShowMenu) {
            ArgSalidaTemp.push('menu')
          }
          return ArgSalidaTemp
  }

  getPageDetails = () => {
    return new Promise((resolve, reject) => { 
      try {
        this.getPageSize().then( async (res:any) => {              
              this.getData(0, res.pageSize).then(() => {
                resolve(true)
              }).catch((err) => console.error(err));
          
            }).catch((err) => console.error(err)); 
      } catch (error) {
        reject(error)
      }          
    })
    
  }

   getPageSize = () => {
      return new Promise((resolve, reject) => {
        resolve(10);
      });
  }

  getData = (pg, lmt) => {
    // console.log(pg, lmt)
    return new Promise((resolve, reject) => {  
      if (lmt != undefined) {
        try {
            this.allProjects(pg, lmt).then( res => {
                  this.tableData = res;
                  resolve(true)

                }).catch((err) => console.error(err));
        } catch (error) {
          reject(error)
        }
              
      } else resolve(true)
    })
    
     
  }

  allProjects = (page, limit) => {    
    return new Promise((resolve, reject) => { 
        try {
          if (limit != undefined) { 
            resolve(this.Data.limit(limit).skip(page * limit))  
          } else {
            reject("limit is undefined")
          }
        } catch (error) {
          reject(Error)
        }
        
    })
    
    
  }

  showPageEvent(pageEvent: PageEvent) {    
    try {      
      //console.log(pageEvent)  
      this.testPaginator = pageEvent;
      this.dataSource.data = this.Data.slice(pageEvent.pageIndex*pageEvent.pageSize,pageEvent.pageSize*(pageEvent.pageIndex+1));  
    } catch (error) {
      console.log(error)
    }    
  } 

  GetOnlyOneKeyReduce(Json: any, KayName: string) {

    let reduce = (array: Array<any>, entry: any): Array<any> => [...array, entry[KayName]];


    let ArgSalida = Json.reduce(reduce, new Array<any>());

    // Busco los elementos que no se quiere mostrar
    Json.forEach((element, index) => {
      if (!element.showlist) {
        console.log(element.KeyJSON + "No se deberia mostrar")
        ArgSalida.splice(index, 1);
      }
    });


    return ArgSalida
  }


  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes.Param.currentValue)

    this.Param = changes.Param.currentValue
    this.Title = changes.Param.currentValue.Title;
    this.bShowMenu = changes.Param.currentValue.Menu

    //console.log(changes.Param.currentValue)
    this.lTypeData = this.GetOnlyOneKeyReduce(changes.Param.currentValue.ConfigCampos, 'TypeDato')
    this.HeadShow = this.GetOnlyOneKeyReduce(changes.Param.currentValue.ConfigCampos, 'HeadShowList')
    this.ldisplayedColumns = this.GetOnlyOneKeyReduce(changes.Param.currentValue.ConfigCampos, 'KeyJSON')
    this.FlexSize = this.GetOnlyOneKeyReduce(changes.Param.currentValue.ConfigCampos, 'flex')

    if (this.bShowMenu) {
      this.lTypeData.push('menu')
      this.HeadShow.push('menu')
      this.ldisplayedColumns.push('menu')

    }
    this.cShowResponsive = this.AnalizadorHead_Type();
    console.log(changes.Param.currentValue.ArgMenu)
    this.Menu = changes.Param.currentValue.ArgMenu

  }



  ClickMenu(Dato, opcion, EditDelete) {
    console.log(Dato)
    console.log(opcion)
    console.log(EditDelete)

    // Controlo el Editar y Eliminar
    if (opcion == 2 && EditDelete == 'new' && this.Title == "Facturas") {
      this.factura = Dato
      this.invoice = true
      this.drawer.open();
      //TODO: falta enviar que es new para que abra el add-edit por defecto
    } else if (opcion == 2 && EditDelete == 'segment') {
      this.invoice = true
      this.drawer.open();

    } else {
      this.invoice = false
    }

    if (EditDelete == 'edit' || EditDelete == 'delete' || EditDelete == 'new') {
      switch (EditDelete) {
        case 'new':

        // Tomar en cuenta los Select que deben estas listados
          this.DataForm = []
          this.Param.ConfigCampos.forEach(element => {
            const DatosSendForm: iDatosForm = {
              ID: element.KeyJSON,
              Label: element.HeadShowList,
              Type: element.TypeForm,
              Data: '',  // En caso de tener un select
              Value: '', // Es un nuevo registro
              Required: element.Required,
              showlist: element.showlist,
              ErrorLabel: element.ErrorShowForm
            }
            this.DataForm.push(DatosSendForm)

          });
          this.drawer.open();
          break;
        case 'edit':
          this.DataForm = []
          this.Param.ConfigCampos.forEach((element: any) => {
            const DatosSendForm: iDatosForm = {
              ID: element.KeyJSON,
              Label: element.HeadShowList,
              Type: element.TypeForm,
              //Data: element.Data || '',  // En caso de tener un select
              Data: Dato[element.KeyJSON] || '',  // En caso de tener un select
              Value: Dato[element.KeyJSON],
              Required: element.Required,
              showlist: element.showlist,
              ErrorLabel: element.ErrorShowForm
            }
            this.DataForm.push(DatosSendForm)

          });
          console.log(this.DataForm)
          this.drawer.open();
          break;
        case 'delete':
          //Delete
          this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: true
          });
          this.confirmDialogRef.componentInstance.confirmMessage = `De ${this.Title} ¿Deseas Eliminar el registro ${Dato[this.Param.ConfigCampos[1].KeyJSON]}?`;

          this.confirmDialogRef.afterClosed()
            .subscribe(result => {

              if (result) {
                console.log(result)
                console.log(this.dataSource)
                let DeleteCampo: boolean = false
                let DataTemp = this.dataSource
                this.Data.forEach((element, index) => {
                  const CampoJson: string = this.Param.ConfigCampos[0].KeyJSON
                  const CampoJson1: string = this.Param.ConfigCampos[1].KeyJSON

                  //console.log(element[CampoJson])
                  //console.log(this.dataSource[index])
                  //console.log(Dato)

                  if (this.dataSource[index]!=undefined&&this.dataSource[index].hasOwnProperty('Avater')&&this.dataSource[index].Avater == Dato.Avater) {

                    DataTemp.splice(index, 1);
                    DeleteCampo = true
                  }
                });
                // Si eimina perfecto pero no refresca en el HTML
                this.dataSource = DataTemp;
                console.log("DataTemp");
                console.log(DataTemp);
                //this.DataForm.push(DataTemp)
              }


              console.log(this.dataSource)
              this.confirmDialogRef = null;

            });
          break;
        default:
          break;
      }
    } else {   // Le indico la opcion seleccionada
      this.DataClick.emit({
        Data: Dato,
        Option: opcion
      });
    }

  }


  ngOnInit() {
    console.log(this.Data)
    // this.dataSource = this.Data//Data//new FilesDataSource(null, this.paginator, this.sort, Data);    
    this.dataSource = new MatTableDataSource(this.Data.slice(0,10));
    this.testPaginator.length = this.Data.length;
    this.getPageDetails().then(()=> {}).catch((err) => console.error(err));
  }

  chooseColor(Index) {	 
        const Salida =   this.BackColor     
        if (this.ldisplayedColumns.length-1 == Index) { // Cambio de Color
            
        
              this.ContadorColor = 0;
              if (this.BackColor == "#D6EAF8") { 
                this.BackColor = "#fff";  
              } else {
                this.BackColor = "#D6EAF8"; 
              } 
          

        } 
        
        
        return Salida
  }

  AnalizadorHead_Type(): any {
    let dataSalida: any = [];
    var yaPase = false;
    this.lTypeData.forEach((element, i) => {
      if (i < 4) {
        switch (element) {
          case 'smallint':
            dataSalida.push('show')
            break;
          case 'string':
            // Esto me permite mostrar solo el primero de tipo string en el reporte
            yaPase ? dataSalida.push('xs') : dataSalida.push('show');
            yaPase = true;
            break;
          case 'status':
            // Esto me permite mostrar solo el primero de tipo string en el reporte
            yaPase ? dataSalida.push('xs') : dataSalida.push('show');
            yaPase = true;
            break;
          case 'img':
            dataSalida.push('xs')
            break;
          case 'avatar':
            dataSalida.push('xs')
            break;
          case 'menu':
            dataSalida.push('show')
            break;
          case 'checkbox':
            dataSalida.push('show')
            break;
          default:
            yaPase ? dataSalida.push('xs') : dataSalida.push('show');
            yaPase = true;
        }
      } else {
        if (element == 'checkbox') { dataSalida.push('show') }
        else if (element == 'menu') { dataSalida.push('show') }
        else {
          switch (i) {
            case 3:
            case 4:
              dataSalida.push('sm')
              break;
            case 5:
            case 6:
              dataSalida.push('md')
              break;
            case 7:
            case 8:
              dataSalida.push('lg')
              break;
          }
        }
      }
    });
    return dataSalida;
  }

  /**
   * Toggle the drawer open
   *
   * @param drawerName
   */
  toggleDrawerOpen(drawerName): void {
    const drawer = this._fuseDrawerService.getComponent(drawerName);
    drawer.toggle();
  }

  SalidaForm(event) {
    console.log("SalidaForm");
    console.log(event);
    this.drawer.close();
    let data = event.data
    console.log(data)

    this.DataClick.emit({
      Data: data,
      Option: data.Option
    });

  }


  applyFilter(event) {



    let filterValue = event.target.value
    console.log(filterValue)
    if (event.keyCode == this.enter) {

      if (filterValue == "" || filterValue == undefined) {
        //this.Data = this.DataALL
        this.esEnter = false   
        console.log(this.testPaginator.pageIndex,this.testPaginator.pageSize)     
        this.dataSource = new MatTableDataSource(this.Data.slice(0,this.testPaginator.pageSize));
      } else {
        // this.dataSource.filter = this.filter.nativeElement.value;
        this.ldisplayedColumns.forEach((element, i) => {

          console.log(element)
          var DataFind = this.getDatoGeneric(filterValue, element)

          if (DataFind.length != 0) {
            // this.Data = DataFind
            this.dataSource = DataFind
          }
        });

      }
    }
    this.esEnter = false
  }

  SalidaInvoice(event) {
    console.log("SalidaInvoice")
    if (event.exit) {
      this.invoice = false
    }
  }

  getDatoGeneric(filterValue: string, Colum: string) {
    let Salida: any = []

    this.Data.forEach(function (element, index) {

      try {
        if (element[Colum] && element[Colum] != '') {
          // console.log(this.Data)
          const value = element[Colum].toLowerCase()
          console.log(value)
          if (value.search(filterValue.toLowerCase()) != -1) {
            Salida.push(element)
          }
        } else {
          console.log(element)
        }
      } catch (error) {
        console.log(error)
      }


    })

    if (Salida.length === 0) {
      return [];
    } else {
      return Salida
    }
  }


}





