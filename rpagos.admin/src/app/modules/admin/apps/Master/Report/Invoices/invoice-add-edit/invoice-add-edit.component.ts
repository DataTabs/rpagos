import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ContactsService} from 'app/modules/admin/apps/contacts/contacts.service';
import {FacturaListService} from 'app/services/request/facturas/factura-list.service';
import {Factura} from 'models/factura.model';
import { InvoicesService } from '../../../../../../../services/request/invoices/invoices.service';

@Component({
  selector: 'app-invoice-add-edit',
  templateUrl: './invoice-add-edit.component.html',
  styleUrls: ['./invoice-add-edit.component.scss']
})
export class InvoiceAddEditComponent implements OnInit, OnChanges{

  @Output() Salida: EventEmitter<any> = new EventEmitter
  @Input() Fact:Factura

  showBill=false
  showService:Boolean[]=[]
  showTax:Boolean[]=[]
  showDiscount:Boolean[]=[]

  empresa: any = {}
  factura: Factura
  bill: any = {}
  allBills: any = [
    {name:"Jose Perez", address:"address1", phone:"123", email:"email1@email.com" },
    {name:"Juan Angulo", address:"address2", phone:"456", email:"emailw@email.com" },
    {name:"Pedro Pedrozo", address:"address3", phone:"789", email:"email3@email.com" },
  ]
  clientes = [
    {

    },
  ]
  ShowInvoice: any = []
  indexInvoice = 0
  limitInvoice = 1

  services = [
    {
      service: "Prototype & Desing",
      description: "Prototyping of the application's general workflow and the detailed design of its 72 screens as a working prototype.",
      price: 75,
    },
    {
      service: "Development",
      description: "The web and mobile development of the application using appropriate tools and registering it to major application stores.",
      price: 60,
    },
    {
      service: "Testing",
      description: "Extensive testing of the application using various mobile and desktop devices with different operating systems.",
      price: 25,
    },
    {
      service: "Documentation & Training",
      description: "Development of the detailed documentation, preparing guides and instructional videos and extensive training of 2 people.",
      price: 26.50,
    },
  ]

  taxesContent = [
    {
      name: "Tax 1",
      description: "desc tax 1",
      amount: 5
    },
    {
      name: "Tax 2",
      description: "desc tax 2",
      amount: 7
    },
    {
      name: "Tax 3",
      description: "desc tax 3",
      amount: 3
    },
  ]

  discountsContent = [
    {
      name: "Discount 1",
      description: "desc disc 1",
      amount: 3
    },
    {
      name: "Discount 2",
      description: "desc disc 2",
      amount: 1
    },
    {
      name: "Discount 3",
      description: "desc disc 3",
      amount: 10
    },
  ]

  items: any = []
  taxes: any = []
  discounts: any = []

  constructor( 
    private invoiceService: InvoicesService,
    private _contactsService: ContactsService,
    private _facturaListService:FacturaListService
  ) {


    this.ShowInvoice[this.indexInvoice] = true;

    //Empresa
    this.empresa.name = "Fuse Inc."
    this.empresa.address1 = "2810 Country Club Road"
    this.empresa.address2 = "Cranford, NJ 07016"
    this.empresa.phone = "+66 123 455 87"
    this.empresa.email = "email@test.com"
    this.empresa.pag = "www.fuseinc.com"
    this.empresa.logo = "assets/images/logo/logo32.png"

    //Cobrar A
    //this.bill.name = "Enrique Cheang"
    //this.bill.address = "9301 Wood Street"
    //this.bill.phone = "+55 552 455 87"
    //this.bill.email = "E.Cheang@company.com"

    this._contactsService.getClientes().subscribe(contacts => {
      //console.log(contacts)
      if (!contacts) return
      contacts.forEach((contact:any) => {
        let datosPush={
          id: contact._id,
          name: contact.ClienteName,
          address:'',
          address2:'',
          phone:'',
          email:'',
        }
        contact.rpClientesExtras.forEach((extra:any)=>{
          console.log(extra)
          switch (extra.TipoDato) {
            case 1:
              datosPush.address=extra.Descrip
              break;
            case 2:
              datosPush.email=extra.Descrip
              break;
            case 3:
              datosPush.phone=extra.Descrip
              break;
            default:
              break;
          }
        })

        this.allBills.push(datosPush)
        //console.log(this.allBills)

        //TODO: aqui hay que recorrer el rpClientesExtras y agregar los datos phone, email, etc
      })
    //{name:"Jose Perez", address:"address1", phone:"123", email:"email1@email.com" },
    })

    /*       this.items.forEach(service =>{
              this.items.push(service)
          }) */

    /*       this.taxesContent.forEach(tax =>{
              this.taxes.push(tax)
          }) */

    /*       this.discountsContent.forEach(disc =>{
              this.discounts.push(disc)
          }) */
    this._facturaListService.getServicesByEmpresa('13').subscribe(service => {
      if (!service) return
      service.forEach((ser:any) => {
      this.setService(ser)
      })
      console.log(this.services)
    })

  }

  ngOnInit(): void {
    this.factura = {
      header: {
        empresa: {
          name: this.empresa.name,
          address1: this.empresa.address1,
          address2: this.empresa.address2,
          phone: this.empresa.phone,
          email: this.empresa.email,
          pag: this.empresa.pag,
          logo: this.empresa.logo,
        },
        bill: {
          name: this.bill.name,
          address1: this.bill.address,
          phone: this.bill.phone,
          email: this.bill.email,
        },
        info: {
          fact_number: "59874563",
          Date: new Date(),
          DueDate: new Date(),
          Total_Due: 4545698
        }
      },
      content: {
        services: this.items,
        taxes: this.taxes,
        Discounts: this.discounts
      },
      footer: {
        subTotal:0,
        total:0,
        Message:"Please pay within 15 days. Thank you for your business.",
        AdditionalInfo:"In condimentum malesuada efficitur. Mauris volutpat placerat auctor. Ut ac congue dolor. Quisque scelerisque lacus sed feugiat fermentum. Cras aliquet facilisis pellentesque. Nunc hendrerit quam at leo commodo, a suscipit tellus dapibus. Etiam at felis volutpat est mollis lacinia. Mauris placerat sem sit amet velit mollis, in porttitor ex finibus. Proin eu nibh id libero tincidunt lacinia et eget."
      }
    }
  }


  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
    this.factura=this.Fact
    this.items=this.factura?.content?.services || []
    this.taxes=this.factura?.content?.taxes|| []
    this.discounts=this.factura?.content?.Discounts|| []

  }

  newItem() {
    this.items.push({
      service: "",
      description: "",
      qty:0,
      price: 0
    })
    this.showService.push(false)
  }

  newTax() {
    this.taxes.push({
      name: "",
      description: "",
      amount: 0,
    })
    this.showTax.push(false)
  }

  newDisc() {
    this.discounts.push({
      name: "",
      description: "",
      amount: 0,
    })
    this.showDiscount.push(false)

  }

  deleteItem(i: number) {
    console.log(i)
    this.items.splice(i, 1)
    this.sumSubtotal()
    this.showService.splice(i, 1)
    //console.log(this.items)
  }

  deleteTax(i: number) {
    console.log(i)
    this.taxes.splice(i, 1)
    this.showTax.splice(i, 1)
    //console.log(this.taxes)
  }

  deleteDisc(i: number) {
    console.log(i)
    this.discounts.splice(i, 1)
    this.showDiscount.splice(i, 1)
    //console.log(this.taxes)
  }


  checkService(item: {service: string; description: string; price: number; qty:number},i:number) {
    /*     console.log("me llego este tax ",item,i ) */
    let finded = this.services.find(s => s.service == item.service)
    if (finded) {
      item.description = finded.description
      item.price = finded.price
      item.qty = 1
      this.sumSubtotal()
      this.showService[i]=true
    }
    this.getTotal()
  }

  checkTax(tax: {name: string; amount: number; description: string;},i:number) {
    //console.log("me llego este tax ",tax,i )
    let finded = this.taxesContent.find(t => t.name == tax.name)
    if (finded) {
      tax.name = finded.name
      tax.amount = finded.amount
      tax.description = finded.description
      this.sumTaxes()
      this.showTax[i]=true
    }
    this.getTotal()
  }

  checkDisc(disc: {name: string; amount: number; description: string;},i:number) {
    //console.log("me llego este tax ",tax,i )
    let finded = this.discountsContent.find(d => d.name == disc.name)
    if (finded) {
      disc.name = finded.name
      disc.amount = finded.amount
      disc.description = finded.description
      this.sumDiscount()
      this.showDiscount[i]=true
    }
    this.getTotal()
  }

  goBack(value: any) {
    this.Salida.emit(true)

  }
  next() {
    console.log("Siguente")
    this.ShowInvoice[this.indexInvoice] = false
    if (this.indexInvoice == this.limitInvoice) {
      this.indexInvoice = 0
    } else {
      this.indexInvoice++
    }
    this.ShowInvoice[this.indexInvoice] = true;
  }

  Previous() {
    console.log("Anterior")
    this.ShowInvoice[this.indexInvoice] = false
    if (this.indexInvoice == 0) {
      this.indexInvoice = this.limitInvoice
    } else {
      this.indexInvoice--
    }
    this.ShowInvoice[this.indexInvoice] = true;

  }

  sumSubtotal() {
    let total = this.items.reduce((sum, item) => (sum+item.price)*(item.qty),0)
    this.factura.footer.subTotal=total
  }

  sumTaxes() {
    //console.log("Taxes", this.taxesContent)
    let total = this.taxes.reduce((sum, item) => sum+item.amount,0)
    this.factura.footer.taxTotal=total
  }

  sumDiscount() {
    //console.log("discounts", this.discountsContent)
    let total = this.discounts.reduce((sum, item) => sum+item.amount,0)
    this.factura.footer.discountTotal=total
  }

  getTotal() {
    //subtotal - taxes - discounts
    this.sumSubtotal()
    this.sumTaxes()
    this.sumDiscount()
    let total:any = 0
    total= Number(this.factura.footer.subTotal) + Number(this.factura.footer.taxTotal)
    //console.log(total)
    total =  total - Number(this.factura.footer.discountTotal)
    //console.log(total)
    this.factura.footer.total= total
  }


  saveInvoice() {
    console.log("Deberia guardar los ajustes del invoice")
    console.log(this.factura)

    this.Salida.emit({
      save:true,
      factura:this.factura
    });
/*     this.invoiceService.createFactura(this.invoiceData())
      .subscribe((res) => {
        console.log(res);
      }) */
  }

  CheckBill(bill:any){
    let newBill = this.allBills.find((b:any)=>b.name == bill.name)
    //console.log(newBill)
    if(newBill){
      this.factura.header.bill.name=newBill.name
      this.factura.header.bill.address1=newBill.address
      this.factura.header.bill.phone=newBill.phone
      this.factura.header.bill.email=newBill.email
      this.showBill= true
    }
    //console.log(bill)
  }

  setService(service:any){
    this.services.push({
      service:service.DescripTitulo,
      description:service.DescripBody,
      price:service.Monto1
    })

    //{
      //service: "Prototype & Desing",
      //description: "Prototyping of the application's general workflow and the detailed design of its 72 screens as a working prototype.",
      //price: 75,
    //},

  }

  invoiceData() {
    // DATA ESTATICA PARA LA CREACION DE UNA  FACTURA
    // CAMBIAR DATOS ANTES DE HACER CADA PRUEBA
    // NO MANADAR DATOS EN idFacturas, idFacturaEstados, idFacturas_Detalle, idFacturaTaxDescuentos
    // MANDARLOS COMO STRING '' VACIOS
    return  {
        "idFacturas": "",
        "idEmpresa": "2",
        "idCliente": "0",
        "idUser": "8CV8ASluvmL4OPv",
        "NumFactura": "N-874s5s",
        "NumControl": "93511",
        "FechaFactura": "2021-11-07",
        "SubTotal": 0,
        "MontoTotal": 0,
        "FechaVencimiento": "2021-11-07",
        "EstadoFactura": 2,
        "rpFacturaEstados": [
            {
                "idFacturaEstados": "",
                "idFacturas": "",
                "Descripcion": "varios datos"
            },
            {
                "idFacturaEstados": "",
                "idFacturas": "",
                "Descripcion": "multiples datos"
            }
        ],
        "rpFacturas_Detalles": [
            {
                "idFacturas_Detalle": "",
                "idFacturas": "",
                "idServicio": "54s-dSH",
                "Descripcion": "multiples detalles de la factura",
                "Cantidad": 9922,
                "Monto": "15",
                "idExtraCliente": 0
            },
            {
                "idFacturas_Detalle": "",
                "idFacturas": "",
                "idServicio": "sGHS-asj",
                "Descripcion": "multiples detalles de la factura",
                "Cantidad": 8888,
                "Monto": "26",
                "idExtraCliente": 0
            }
        ],
        "rpFacturaTaxDescuentos": [
            {
                "idFacturaTaxDescuentos": "",
                "idFacturas": "",
                "tipo": 3,
                "Monto": "8793"
            },
            {
                "idFacturaTaxDescuentos": "",
                "idFacturas": "",
                "tipo": 1,
                "Monto": "9558"
            }
        ]
    }
  }


}
