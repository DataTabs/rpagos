import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { iDatosForm } from '../../../DatosListGeneric';

@Component({
    selector: 'app-form-generator',
    templateUrl: './form-generator.component.html',
    styleUrls: ['./form-generator.component.scss']
})
export class FormGeneratorComponent implements OnInit {
    @Input() data: iDatosForm[] = []
    @Input() title: any = ''
    @Input() accion: any = 'Guardar'
    @Output() dataSalida: EventEmitter<any> = new EventEmitter();

    setForm:any=[]

    dynamicFormArray: any
    AvatarShow: boolean = false;
    Tipo: string[] = ["String", "E-mail", "Número", "Teléfono", "Fecha", "Name", "Cédula/DNI", "Tarjeta Crédito", "string", "input", "status"];
    Prefijo: String = 'Generico.';
    avatar

    constructor(private httpClient: HttpClient) { }

    ngOnInit() {
        //console.log('esto me llego al input del generator', this.data)
        this.dynamicFormArray = this.data
/*              this.dynamicFormArray=testForm */
    }

    ngOnChanges(changes: SimpleChanges): void {
        // por favor quita esta seleccion que me pone un color morado loco
        console.log('detecte un cambio en formGenerator:', changes, this.data)
        console.log(changes)

        this.dynamicFormArray=changes.data.currentValue
        if(this.data.length>0){
            if(this.data[0].Value==""){
                this.data['Option']=""
            }else{
                this.data['Option']=0;
            }
        }
        

        

        // Busco si dentro de los elementos tiene Avatar
        for (let index = 0; index < changes.data.currentValue.length; index++) {
            const element = changes.data.currentValue[index];
            if (element.Type === 'avatar'){
                 this.AvatarShow = true
                 this.avatar=element.Value
             break
            }

        }

    }

    clg() {
/*        console.log(this.data) */
        let actAvatar= this.data.find((row:any)=>row.Type=='avatar')
        if (actAvatar) {
            actAvatar.Value=this.avatar
        }
        console.log("El form emitio");
        console.log(this.data);
        this.dataSalida.emit({ data: this.data })
/*         this.dataSalida.emit({ data: testForm }) */
    }

        uploadAvatar(fileList: FileList): void
    {
        console.log(fileList)
        // Return if canceled
        if ( !fileList.length )
        {
            return;
        }

        const allowedTypes = ['image/jpeg', 'image/png'];
        const file = fileList[0];

        // Return if the file is not allowed
        if ( !allowedTypes.includes(file.type) )
        {
            return;
        }

        this.avatar=file
    }

        uploadFile($event) {
        const ResumValue = $event.target.files[0]
        //console.log($event.target.files[0]); // outputs the first file

        var reader = new FileReader();

        reader.readAsDataURL($event.target.files[0]);
        reader.onload = (_event) => {
            //TODO: No quiere cambiar la img en el front, en el json ya lo hace
            this.avatar=reader.result
/*             console.log(this.avatar) */
        }
    }

}

export const testForm = [
    {
        "ID": "idUser",
        "Label": "idUser",
        "Type": "input",
        "Data": "",
        "Value": "",
        "Required": false,
        "showlist":false
    },
    {
        "ID": "FirstName",
        "Label": "First Name",
        "Type": "input",
        "Data": "",
        "Value": "Pedro",
        "Required": true,
        "showlist":true
    },
    {
        "ID": "Apellido",
        "Label": "Last Name",
        "Type": "input",
        "Data": "",
        "Value": "Perez",
        "Required": true,
        "showlist":true
    },
    {
        "ID": "Address",
        "Label": "Address",
        "Type": "area",
        "Data": "",
        "Value": "Sunt Lorem laborum reprehenderit est consequat.",
        "Required": false,
        "showlist":true
    },
    {
        "ID": "DOB",
        "Label": "DOB",
        "Type": "date",
        "Data": "",
        "Value": "2021-07-02",
        "Required": true,
        "showlist":true
    },
/*     {
        "ID": "Gender",
        "Label": "Gender",
        "Type": "radio",
        "Data": "Male,Female",
        "Value": "Male",
        "Required": false,
        "showlist":true
    }, */
    {
        "ID": "City",
        "Label": "City",
        "Type": "select",
        "Data": "Uno/Dos/Tres/Cuatro/Cinco",
        "Value": ["Uno", "Dos", "Tres"],
        "Required": true,
        "showlist":true
    }
]
