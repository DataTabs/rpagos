import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterListAddComponent } from './MasterListAdd.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from 'app/shared/shared.module';
import { FuseDrawerModule } from '@fuse/components/drawer';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { FuseFindByKeyPipeModule } from '@fuse/pipes/find-by-key';
import { FormGeneratorComponent } from '../form-generator/form-generator.component';
import { FuseConfirmDialogModule } from '@fuse/components/confirm-dialog/confirm-dialog.module';
import { TranslocoModule } from '@ngneat/transloco';
import { AudiofPipe } from '../../../Services/audiof.pipe';
import { InvoiceAddEditComponent } from '../../Invoices/invoice-add-edit/invoice-add-edit.component';
import {MatRadioModule} from '@angular/material/radio';
import { InvoicePreviewComponent } from '../../Invoices/invoice-preview/invoice-preview.component';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatRadioModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    SharedModule,
    TranslocoModule,
    FuseDrawerModule,
    MatSidenavModule,
    MatDialogModule,
    FuseFindByKeyPipeModule,
    FuseConfirmDialogModule,
    MatCardModule
  ],
  exports: [MasterListAddComponent, FormGeneratorComponent,AudiofPipe, InvoiceAddEditComponent, InvoicePreviewComponent],
  declarations: [MasterListAddComponent,FormGeneratorComponent,AudiofPipe, InvoiceAddEditComponent,InvoicePreviewComponent]
})
export class MasterListAddModule { }
