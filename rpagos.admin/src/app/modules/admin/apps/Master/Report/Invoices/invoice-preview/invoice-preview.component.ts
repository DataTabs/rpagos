import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import {isEmpty} from 'lodash';
import {Factura} from 'models/factura.model';

@Component({
  selector: 'app-invoice-preview',
  templateUrl: './invoice-preview.component.html',
  styleUrls: ['./invoice-preview.component.scss']
})
export class InvoicePreviewComponent implements OnInit, OnChanges {
  @Output() Salida: EventEmitter<any> = new EventEmitter
  @Input() Fact:Factura

    edit=false;
    empresa: any={};
    showInvoice=[];
    indexInvoice =0;
    limitInvoice =1;
    factura: Factura;

    show=false

  constructor() {
      this.showInvoice[this.indexInvoice]=true;
   }

  ngOnInit()  {
    if(isEmpty(this.factura) && isEmpty(this.Fact)){
      console.log('no hay datos, vamos a edit', this.factura);
      this.edit=true;
    }
    this.showInvoice[this.indexInvoice] = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);

  }

  print(){
      window.print();

  }

  editFactura(){
    this.factura = this.factura
    this.edit=true
  }

  next() {
      console.log('Siguente');
      this.showInvoice[this.indexInvoice]=false;
      if(this.indexInvoice === this.limitInvoice){
      this.indexInvoice=0;
      }else{
      this.indexInvoice++;
      }
      this.showInvoice[this.indexInvoice]=true;
  }

  previous() {
      console.log('Anterior');
      this.showInvoice[this.indexInvoice]=false;
      if(this.indexInvoice === 0){
      this.indexInvoice=this.limitInvoice;
      }else{
      this.indexInvoice--;
      }
      this.showInvoice[this.indexInvoice]=true;
  }

  salidaEdit(event) {
      console.log(event);
    this.showInvoice[this.indexInvoice] = true;
    this.factura = event.factura;
    console.log(this.factura);
      this.edit=false;
  }

  exit(){
      this.Salida.emit({
          exit:true
      })

  }
  
  SaveInvoice(){
    console.log('guardar factura');
  }

}
