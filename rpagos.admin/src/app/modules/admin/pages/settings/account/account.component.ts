import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from 'app/core/auth/auth.service';
import { UserListService } from '../../../../../services/request/user-list/user-list.service';

@Component({
    selector       : 'settings-account',
    templateUrl    : './account.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsAccountComponent implements OnInit
{
    accountForm: FormGroup;

    DatosUser:any 

    /**
     * Constructor
     */
    constructor(
        private _formBuilder: FormBuilder,
        private authservice: AuthService,
        private userListService: UserListService
    )
    {
        console.log("Datos del token")
        this.DatosUser = this.authservice.getDecodedAccessToken()
        console.log(this.DatosUser)
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.accountForm = this._formBuilder.group({
            name    : [this.DatosUser.NameUser],
            username: [this.DatosUser.LoginUser],
            title   : ['Senior Frontend Developer'],
            company : ['YXZ Software'],
            about   : ['Hey! This is Brian; husband, father and gamer. I\'m mostly passionate about bleeding edge tech and chocolate! 🍫'],
            email   : [this.DatosUser.Email, Validators.email],
            phone   : [this.DatosUser.Movil],
            country : ['usa'],
            language: ['english']
        });
    }

    DataForm(datos:any){
        console.log("entre en datafrom");
        //console.log(datos.value);
        const { name, username, phone } = datos.value;
        //console.log({name, username, phone});
        this.userListService.updateInfoUser(
        {
            idUsers: this.DatosUser.idUsers, 
            NameUser:name, 
            LoginUser: username, 
            Movil:phone
        }).subscribe((res) => {
            console.log(res);
        });
    }

}
