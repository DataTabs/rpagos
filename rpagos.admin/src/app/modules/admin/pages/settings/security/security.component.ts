import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'app/core/auth/auth.service';
import { UserListService } from '../../../../../services/request/user-list/user-list.service';

@Component({
    selector: 'settings-security',
    templateUrl: './security.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsSecurityComponent implements OnInit {
    securityForm: FormGroup;

    /**
     * Constructor
     */
    constructor(
        private _formBuilder: FormBuilder,
        private userList: UserListService,
        private authservice: AuthService

    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Create the form
        this.securityForm = this._formBuilder.group({
            currentPassword: [''],
            newPassword: [''],
            twoStep: [true],
            askPasswordChange: [false]
        });
    }

    DataForm(Dform: any) {
        let email = this.authservice.getDecodedAccessToken().Email;
        console.log("Datos formulario security")
        console.log(Dform.value)
        let dataToSend = { email, oldPass: Dform.value.currentPassword,  newPass: Dform.value.newPassword }; 
        console.log({email, oldPass: Dform.value.currentPassword,  newPass: Dform.value.newPassword });
        this.userList.changePassword(dataToSend.oldPass, dataToSend.newPass, dataToSend.email)
            .subscribe(
            (res) => {
                console.log(res)
            }, (err) => {
                console.log(err.error);
            })
    }

}
