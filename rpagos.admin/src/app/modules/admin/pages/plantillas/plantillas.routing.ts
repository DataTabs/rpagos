/* import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {  },
];

export const PlantillasRoutes = RouterModule.forChild(routes); */
import { Route } from '@angular/router';
import { PlantillasComponent } from './plantillas.component';

export const PlantillasRoutes: Route[] = [
    {
        path     : '',
        component: PlantillasComponent
    }
];
