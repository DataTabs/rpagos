import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { Board } from 'app/modules/admin/apps/scrumboard/scrumboard.models';
import { ScrumboardService } from 'app/modules/admin/apps/scrumboard/scrumboard.service';

@Component({
  selector: 'app-plantillas',
  templateUrl: './plantillas.component.html',
  styleUrls: ['./plantillas.component.scss']
})
export class PlantillasComponent implements OnInit {
    boards:any

    // Private
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
  constructor() {
      this.boards=[
          {id:1,img:"https://legaldbol.com/wp-content/uploads/2019/03/82-Html-Invoice-Template-For-Email-Now-by-Html-Invoice-Template-For-Email.jpg"},
          {id:2,img:"https://1stwebdesigner.com/wp-content/uploads/2017/10/free-invoice-template-creatives-03.jpg"},
          {id:3,img:"https://www.cmsbased.net/cleanhtml-invoice.svg"},
      ]
  }

    ngOnInit(): void
    {
        // Get the boards
    }

    clg(value){
        console.log(value)
    }

}

