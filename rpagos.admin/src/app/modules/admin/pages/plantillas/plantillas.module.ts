import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlantillasComponent } from './plantillas.component';
import { RouterModule } from '@angular/router';
import { PlantillasRoutes } from './plantillas.routing';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SharedModule } from 'app/shared/shared.module';
import { PlantillaComponent } from './plantilla/plantilla.component';
import { ListPlantillasComponent } from './list-plantillas/list-plantillas.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PlantillasRoutes),
        DragDropModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatMomentDateModule,
        MatProgressBarModule,
        SharedModule
  ],
  declarations: [PlantillasComponent, PlantillaComponent, ListPlantillasComponent]
})
export class PlantillasModule { }
