import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { ClientesRequestService } from '../../../../services/request/clientes/clientes-request.service';


@Component({
    selector       : 'profile',
    templateUrl    : './profile.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent
{
    /**
     * Constructor
     */
    constructor( private clientesRequestServices: ClientesRequestService )
    {
    }

    getInfoCliente() {
        this.clientesRequestServices.getClienteByPk('1').subscribe((res) => {
            console.log(res);
        })
    }

}
