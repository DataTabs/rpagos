import { ChangeDetectionStrategy, Component, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs';
import { MatSidenav } from '@angular/material/sidenav';
import { FuseDrawerService } from '@fuse/components/drawer';
import { DatosListGeneric } from 'app/modules/admin/apps/Master/DatosListGeneric';

// SERVICES
import { InvoicesService } from '../../../../../../services/request/invoices/invoices.service';
import { ClientesRequestService } from '../../../../../../services/request/clientes/clientes-request.service';

import { Factura } from '../factura.type';

import { FacturaListService } from 'app/services/request/facturas/factura-list.service';

@Component({
    selector: 'modern',
    templateUrl: './modern.component.html',
    styleUrls: ['./modern.component.scss'],
    // encapsulation: ViewEncapsulation.None,
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModernComponent {

    showTable: boolean = false
    showTable2: boolean = false

    setForm: any = []
    dataTable: any = [] 

    @ViewChild('drawer', { static: false })
    drawer: MatSidenav;

    public Parametros: DatosListGeneric = {
        Title: "Facturas",          // Titulo para mostrar
        TableName: "facturas",      // Nombre de la Tabla en Base de Datos / Inmemory
        Menu: true,                 // Indica si la lista tiene Menú
        MultiSelect: false,         // Seleccion multiple en la lista
        ArgMenu: ["Editar/edit", "Eliminar/delete", "Factura/segment", "PPP/segment"], //  // Menú de Opciones en la lista
        DownloasExcel: true,
        ConfigCampos: [/* {
            TypeDato: 'avatar',     // Tipo de Datos para la Lista {string, phone, date, icon, avatar, img}
            KeyJSON: 'avatar',      // El canpo que viene en el json
            HeadShowList: 'Avatar', // Title de la lista
            TypeForm: 'avatar', // Tipo de Dato para el Formulario
            Required: false,        // Si el campo es requerido para el Form
            showlist: false,         // Si se va a mostrar en la lista
            IndexOrderList: 0,      // En que orden va salir en la lista
            IndexOrderForm: 0,      // En que orden va salir en el Formulario
            ErrorShowForm: ''       // Texto en caso de Error
        },  */{
                TypeDato: 'string',
                KeyJSON: 'ClienteName',
                HeadShowList: 'Cliente',
                TypeForm: 'input',
                Required: true,
                showlist: true,
                IndexOrderList: 0,
                IndexOrderForm: 0,
                ErrorShowForm: 'Nombre Cliente es Requerido', 
                flex: 'flex: 0 0 15%;'
            }, {
                TypeDato: 'string',
                KeyJSON: 'NumFactura',
                HeadShowList: 'Nro. Factura',
                TypeForm: 'input',
                Required: true,
                showlist: true,
                IndexOrderList: 0,
                IndexOrderForm: 0,
                ErrorShowForm: 'Agente es Requerido', 
                flex: 'flex: 0 0 15%;'
            }, {
                TypeDato: 'string',
                KeyJSON: 'FechaFactura', //AGREGAR TIPO DE DATO 'DATE'
                HeadShowList: 'Fecha',
                TypeForm: 'input',
                Required: false,
                showlist: true,
                IndexOrderList: 0,
                IndexOrderForm: 0,
                ErrorShowForm: 'Cola es Requerido', 
                flex: 'flex: 0 0 15%;'
            }, {
                TypeDato: 'string',
                KeyJSON: 'NumControl',
                HeadShowList: 'NumControl',
                TypeForm: 'input',
                Required: false,
                showlist: true,
                IndexOrderList: 0,
                IndexOrderForm: 0,
                ErrorShowForm: 'Estatus es Requerido', 
                flex: 'flex: 0 0 15%;'
            }
            , {
                TypeDato: 'string',
                KeyJSON: 'MontoTotal',
                HeadShowList: 'Monto',
                TypeForm: 'input',
                Required: false,
                showlist: true,
                IndexOrderList: 0,
                IndexOrderForm: 0,
                ErrorShowForm: 'Monto es Requerido', 
                flex: 'flex: 0 0 15%;'
            }
        ]
    }

    public allList: any[] = [];
    constructor(
        private _fuseDrawerService: FuseDrawerService,
        private invoicesService: InvoicesService,
        private clientesServices: ClientesRequestService,
        private facturalistservice: FacturaListService
    ) {
        this.GenerateTable()
    }

    ngOnInit() {

    }

    GenerateTable() {
        try {
            combineLatest([
                this.invoicesService.getAllInvoicesByIdEmpresa('12'),
                this.clientesServices.getClientesAll('12')
            ]).subscribe(([invoicesByEmpresa, clientesByEmpresa]) => {
                this.BuildData(clientesByEmpresa, invoicesByEmpresa)
            })

        } catch (error) {
            console.error(error)
        }
    }

    DataSalida(event) {
        console.log("esto me llego: ", event)
        //Esto es a modo de prueba
        this.showTable2 = true
        this.drawer.open();
        //Fin
        switch (event.Option) {
            case 0:
                //Edit
                /*                 this.createForm(event.Data) */

                break;
            case 1:
                //Delete
                console.log('delete: ', event.Data)
                break;

            default:
                break;
        }
    }

    createForm(datos: any) {
        datos.forEach(d => {
            /*             this.setForm.push({
                    "ID": "idUser",
                    "Label": "idUser",
                    "Type": "input",
                    "Data": "",
                    "Value": "",
                    "Required": false,
                    "showlist":true
                         } ) */

        });

    }

    BuildData(DatosClientes: any, DatosFacturas: any) {
        try {

            let Cliente: any

            console.log("Clientes")
            console.log(DatosClientes)

            console.log("Facturas")
            console.log(DatosFacturas)

            if (DatosFacturas.length > 0) {

                for (let indexFacturas = 0; indexFacturas < DatosFacturas.length; indexFacturas++) {
                    Cliente = DatosClientes.find((Client: any) => Client.idCliente === DatosFacturas[indexFacturas].idCliente)

                    if (Cliente != undefined) { 
                            const DataFacturaFinal = {
                                ClienteName: Cliente.ClienteName,
                                NumFactura: DatosFacturas[indexFacturas].NumFactura,
                                FechaFactura: DatosFacturas[indexFacturas].FechaFactura,
                                NumControl: DatosFacturas[indexFacturas].NumControl,
                                MontoTotal: DatosFacturas[indexFacturas].MontoTotal,
                                Factura: DatosFacturas[indexFacturas]
                            }

                            this.dataTable.push(DataFacturaFinal)
                    }
                    

                    if (indexFacturas === DatosFacturas.length - 1) {
                        this.showTable = true
                        console.log("Ya deberia de mostrar la tabla " + this.showTable)
                        console.log(this.dataTable)
                    }

                }

            }

        } catch (error) {
            console.error(error)
        }
    }


}
