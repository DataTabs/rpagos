import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MasterListAddModule } from 'app/modules/admin/apps/Master/Report/MasterListAdd/MasterListAdd/MasterListAdd.module';
import { ModernComponent } from 'app/modules/admin/pages/invoice/printable/modern/modern.component';
import { modernRoutes } from 'app/modules/admin/pages/invoice/printable/modern/modern.routing';
import { FuseDrawerModule } from '@fuse/components/drawer';
import {MatSidenavModule} from '@angular/material/sidenav';
import { CommonModule } from '@angular/common';  

import { FormControlCobranzaModule } from '../form-control-cobranza/form-control-cobranza.module';

@NgModule({
    declarations: [
        ModernComponent
    ],
    imports     : [
        MasterListAddModule,
        RouterModule.forChild(modernRoutes),
        FuseDrawerModule,
        MatSidenavModule,
        CommonModule,
        FormControlCobranzaModule
    ]
})
export class ModernModule
{
}
