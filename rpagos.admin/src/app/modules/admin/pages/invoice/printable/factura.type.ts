export interface Factura {
    idFacturas:       string;
    idEmpresa:        number;
    idCliente:        number;
    idUser:           number;
    NumFactura:       string;
    NumControl:       string;
    FechaFactura:     Date;
    SubTotal:         number;
    MontoTotal:       number;
    FechaVencimiento: Date;
    EstadoFactura:    Date;
    createdAt:        Date;
    updatedAt:        Date;
    idExtraCliente:   number;
    rpFacturaEstados: FacturaEstado[];
    rpFacturas_Detalles: FacturaDetalle[];
    rpFacturaTaxDescuentos: FacturaDescuento[];
}

export interface FacturaEstado {
    idFacturaEstados: string;
    idFacturas:       string;
    Descripcion:      string;
    createdAt:        Date;
    updatedAt:        Date;
}


export interface FacturaDetalle {
    idFacturas_Detalle: string;
    idFacturas:         string;
    idServicio:         number;
    Descripcion:        string;
    Cantidad:           number;
    Monto:              string;
    idExtraCliente:     number;
    createdAt:          Date;
    updatedAt:          Date;
}


export interface FacturaDescuento {
    idFacturaTaxDescuentos: string;
    idFacturas:             string;
    tipo:                   number;
    Monto:                  string;
    createdAt:              Date;
    updatedAt:              Date;
}
