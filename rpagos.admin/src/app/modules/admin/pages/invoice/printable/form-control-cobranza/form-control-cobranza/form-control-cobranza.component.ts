import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ControlCobranzaService } from 'app/services/request/ControlCobranza/Control-Cobranza.service';

@Component({
  selector: 'app-form-control-cobranza',
  templateUrl: './form-control-cobranza.component.html',
  styleUrls: ['./form-control-cobranza.component.scss']
})
export class FormControlCobranzaComponent implements OnInit {

  ControlForm: FormGroup;

  DataCobranza: any

  TipoCobro: any = ["Inmediato", "5 Dias Antes", "10 Dias Antes", "15 Dias Antes", "Dia Vencimiento"]

  selected:any

  constructor(
    private _formBuilder: FormBuilder,
    private controlcobranzaservice: ControlCobranzaService
  ) {
    this.controlcobranzaservice.getControlCobranzaByFactura('15').subscribe((res: any) => {
      this.DataCobranza = res[0]
      console.log("Data Cobranza")
      console.log(this.DataCobranza)
      this.ControlForm.patchValue(this.DataCobranza)
    })
  }

  ngOnInit(): void {
    this.ControlForm = this._formBuilder.group({
      Email: [''],
      Movil: [''],
      Extra1: [''],
      Extra2: [''],
      Estado: [''],
      FechaCobro: [''],
      DiaCobro:['null']
    });
  }

  DataForm(DataForm) {
    console.log("Data del form")
    console.log(DataForm.value)

    /* const form = DataForm.value;
    this.controlcobranzaservice.updateControlCobranza({
      idControlCobranza: this.DataCobranza.idControlCobranza, 
      ...form
    })
    .subscribe((res) => {
      console.log(res);
    }); */
  }

}
