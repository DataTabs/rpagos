import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormControlCobranzaComponent } from './form-control-cobranza.component';

describe('FormControlCobranzaComponent', () => {
  let component: FormControlCobranzaComponent;
  let fixture: ComponentFixture<FormControlCobranzaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormControlCobranzaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormControlCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
