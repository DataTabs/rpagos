import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControlCobranzaComponent } from './form-control-cobranza/form-control-cobranza.component';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FuseAlertModule } from '@fuse/components/alert';
import { SharedModule } from 'app/shared/shared.module';




@NgModule({
  declarations: [
    FormControlCobranzaComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSidenavModule,
    FuseAlertModule,
    SharedModule
  ],
  exports: [FormControlCobranzaComponent]
})
export class FormControlCobranzaModule { }
