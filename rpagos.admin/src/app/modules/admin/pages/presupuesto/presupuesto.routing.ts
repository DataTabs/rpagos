import { Route } from '@angular/router';
import { PresupuestoComponent } from './presupuesto.component';

export const PresupuestoRoutes: Route[] = [
    {
        path     : '',
        component: PresupuestoComponent
    }
];
 