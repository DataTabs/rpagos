import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PresupuestoComponent } from './presupuesto.component';
import { RouterModule } from '@angular/router';
import { PresupuestoRoutes } from './presupuesto.routing';
import { MasterListAddModule } from 'app/modules/admin/apps/Master/Report/MasterListAdd/MasterListAdd/MasterListAdd.module';

@NgModule({
  imports: [
    MasterListAddModule,
    CommonModule,
    RouterModule.forChild(PresupuestoRoutes),
  ],
  declarations: [PresupuestoComponent]
})
export class PresupuestoModule { }
