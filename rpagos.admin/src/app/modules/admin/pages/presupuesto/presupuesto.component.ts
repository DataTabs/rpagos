import { Component, OnInit } from '@angular/core';
import { DatosListGeneric } from '../../apps/Master/DatosListGeneric';

@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: ['./presupuesto.component.scss']
})
export class PresupuestoComponent implements OnInit {


  dataTable: any = [{
    nombre: "Jhonattan",
    telefono: "34534534534"
    },{
    nombre: "Misael",
    telefono: "234234234",  }]


  public Parametros: DatosListGeneric = {
    Title: "Amigos",          // Titulo para mostrar 
    TableName: "amigos",      // Nombre de la Tabla en Base de Datos / Inmemory
    Menu: true,                 // Indica si la lista tiene Menú
    MultiSelect: false,         // Seleccion multiple en la lista
    ArgMenu: ["Editar/edit", "Eliminar/delete"],  // Menú de Opciones en la lista
    DownloasExcel: true,
    ConfigCampos: [{
        TypeDato: 'string',     // Tipo de Datos para la Lista {string, phone, date, icon, avatar, img}
        KeyJSON: 'nombre',      // El canpo que viene en el json
        HeadShowList: 'Nombre y Apellido', // Title de la lista
        TypeForm: 'input', // Tipo de Dato para el Formulario
        Required: false,        // Si el campo es requerido para el Form
        showlist: true,         // Si se va a mostrar en la lista
        IndexOrderList: 0,      // En que orden va salir en la lista
        IndexOrderForm: 0,      // En que orden va salir en el Formulario
        ErrorShowForm: '',       // Texto en caso de error, 
        flex: 'flex: 0 0 35%;'
    }, {
        TypeDato: 'string',
        KeyJSON: 'telefono',
        HeadShowList: 'telefono',
        TypeForm: 'input',
        Required: true,
        showlist: true,
        IndexOrderList: 0,
        IndexOrderForm: 0,
        ErrorShowForm: 'Nombre es Requerido', 
        flex: 'flex: 0 0 25%;'
    } 
    ]
}
  constructor() { }

  ngOnInit() {
  }

}
